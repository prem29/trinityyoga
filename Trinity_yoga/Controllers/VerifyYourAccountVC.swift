//
//  VerifyYourAccountVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//
import UIKit
import SVPinView
class VerifyYourAccountVC: UIViewController {
    @IBOutlet weak var otpsending: SVPinView!
    @IBOutlet weak var resentbtn: UIButton!
    @IBOutlet weak var verifyaccountlbl: UILabel!
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var timeelbl: UILabel!
    
    var timing = Timer()
    var totalSecond = 60
    var Strchk = ""
    var Email = ""
    var id : Int!
    var sendOtp : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        id = UserDefaults.standard.object(forKey: LoginuserId) as? Int ?? 0
        startTimer()
        submitbtn.setTitle("SUBMIT", for: .normal)
        submitbtn.applyRadialGradient(colours: [buttoncolor,buttoncolor2])
        resentbtn.setTitle("RESENT", for: .normal)
        submitbtn.clipsToBounds = true
        submitbtn.layer.cornerRadius = submitbtn.frame.height / 2
        submitbtn.setTitleColor(UIColor.white, for: .normal)
        resentbtn.setTitleColor(UIColor.black, for: .normal)
        resentbtn.isHidden = true
    let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGasture))
        swipeLeft.direction = .right
        self.view.addGestureRecognizer(swipeLeft)
    }
    @objc func handleGasture(gesture : UISwipeGestureRecognizer){
        if gesture.direction == .right{
            self.navigationController?.popViewController(animated: true)
        }
    }
    func startTimer() {
           timeelbl.isHidden = false
           timing = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
       }
    func apiOtpVerification(){
        let params = ["id" : id! , "otp" : sendOtp!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "verify_otp", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .post, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
                print(response!)
                let status = response?[0]["status"] as? Int ?? 0
                let erro_msg = response?[0]["error_msg"] as? String ?? ""

                if status == 1
                {
                  let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                    vc.isSet = false
                     self.navigationController?.show(vc, sender: nil)
                }
                else
                {
                    UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
                    self.otpsending.clearPin()
                }
            }, with: nil)
    }
    func apiResendOtp(){
        let params = ["id" : id!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "resend_otp", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .post, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
                print(response!)
            }, with: nil)
    }
    @objc func updateTime() {
           print(timeFormatted(totalSecond))
           if totalSecond != 0 {
               totalSecond -= 1
           } else {
               endTimer()
               timeelbl.isHidden = true
               resentbtn.isHidden = false
           }
       }
       func endTimer() {
           timing.invalidate()
       }
       func timeFormatted(_ totalSeconds: Int){
           let seconds: Int = totalSeconds % 60
        timeelbl.text = String(format: "0:%02d", seconds)
       }
    @IBAction func resendaction(_ sender: UIButton) {
        apiResendOtp()
        self.totalSecond = 60
        self.resentbtn.isHidden = true
        self.startTimer()
    }
    @IBAction func submitaction(_ sender: UIButton) {
        sendOtp = Int(otpsending.getPin())
        apiOtpVerification()
    }
    @IBAction func backaction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        timing.invalidate()
    }

}
