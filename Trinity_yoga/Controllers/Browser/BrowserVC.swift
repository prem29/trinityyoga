//
//  BrowserVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
struct BrowserResponse : Decodable{
    let status : Int!
    let error_msg : String!
    let data : [BrowserData]!
}
struct BrowserData : Decodable{
    let id : Int!
    let name : String!
    let status : Int!
}
class BrowserVC: UIViewController {
    
    @IBOutlet weak var tbvBrowser: UITableView!
    var browserArr = [BrowserData]()
    var browser_Id : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.title = "Browse"
        let navLabel = UILabel()
        let navTitle = NSMutableAttributedString(string: "Browse", attributes:[
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "Ubuntu", size: 23)!])
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
        let nib = UINib(nibName: "BrowserCell", bundle: nil)
        tbvBrowser.register(nib, forCellReuseIdentifier: "BrowserCell")
        tbvBrowser.dataSource = self
        tbvBrowser.delegate = self
      }
    override func viewWillAppear(_ animated: Bool) {
        apiGerBrowserIndex()
    }
      override func viewDidDisappear(_ animated: Bool) {
        if self.tabBarController?.selectedIndex != 2{
            self.title = ""
        }
      }
    
    @objc func sidemen()
    {
        self.sideMenuController?.showLeftView(animated: true)
    }
    func apiGerBrowserIndex(){
        ServiceManager.shared.MakeApiCall(ForServiceName: "browse_list", withParameters: nil, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
           // print(response!)
            let status = response?[0]["status"] as? Int ?? 0
            let erro_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1
            {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                    let decode = try JSONDecoder().decode(BrowserResponse.self, from: jsonData)
                    self.browserArr = decode.data!
                    print(self.browserArr)
                    self.tbvBrowser.reloadData()
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            else
            {
                UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
            }
        }, with: nil)
    }
}
extension BrowserVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(browserArr.count)
        return browserArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbvBrowser.dequeueReusableCell(withIdentifier: "BrowserCell", for: indexPath)as! BrowserCell
        cell.lblBwoserName.text = browserArr[indexPath.row].name
        cell.lblBwoserName.backgroundColor = UIColor.white
        cell.lblBwoserName.clipsToBounds = true
        cell.lblBwoserName.layer.borderColor = UIColor.black.cgColor
        cell.lblBwoserName.layer.borderWidth = 0.7
        cell.lblBwoserName.layer.cornerRadius = 8
        cell.lblBwoserName.layer.shadowColor = UIColor.lightGray.cgColor
        cell.lblBwoserName.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.lblBwoserName.layer.shadowRadius = 3
        cell.lblBwoserName.layer.shadowOpacity = 1
        cell.lblBwoserName.layer.masksToBounds = false;
        cell.lblBwoserName.font = UIFont(name: "Ubuntu", size: 21)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tbvBrowser.cellForRow(at: indexPath) as! BrowserCell
        cell.lblBwoserName.backgroundColor = UIColor(red: 183/255, green: 239/255, blue: 239/255, alpha: 1)
        cell.lblBwoserName.font = UIFont(name: "Ubuntu-Bold", size: 21)
        cell.lblBwoserName.layer.borderWidth = 0
        let category_id = browserArr[indexPath.row].id
        UserDefaults.standard.set(category_id, forKey: "category_id")
        print(category_id)
        let topicVC = storyboard?.instantiateViewController(withIdentifier: "TopicsVC")as! TopicsVC
                   self.navigationController?.pushViewController(topicVC, animated: true)
//        browser_Id = browserArr[indexPath.row].id
//        switch browser_Id {
//        case 1:
//            let topicVC = storyboard?.instantiateViewController(withIdentifier: "TopicsVC")as! TopicsVC
//            self.navigationController?.pushViewController(topicVC, animated: true)
//            break
//            case 2:
//
//            break
//            case 3:
//            break
//            case 4:
//            break
//            case 5:
//            break
//        default:
//            print(Error.self)
//        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tbvBrowser.cellForRow(at: indexPath) as! BrowserCell
        cell.lblBwoserName.backgroundColor = UIColor.white
        cell.lblBwoserName.clipsToBounds = true
        cell.lblBwoserName.layer.borderColor = UIColor.black.cgColor
        cell.lblBwoserName.layer.borderWidth = 0.7
        cell.lblBwoserName.layer.cornerRadius = 8
        cell.lblBwoserName.layer.shadowColor = UIColor.lightGray.cgColor
        cell.lblBwoserName.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.lblBwoserName.layer.shadowRadius = 3
        cell.lblBwoserName.layer.shadowOpacity = 1
        cell.lblBwoserName.layer.masksToBounds = false;
        cell.lblBwoserName.font = UIFont(name: "Ubuntu", size: 21)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
