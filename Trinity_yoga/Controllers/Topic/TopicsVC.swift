//
//  TopicsVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//
import UIKit
struct CategoryListResponse : Decodable {
    let error_msg : String!
    let data : [CategoryList]!
    let status : Int!
}
struct  CategoryList : Decodable {
   let id : Int!
   let category_id : Int!
    var name : String!
   let status: Int!
}
class TopicsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
   
    var selectedtopic : String = "default"
    var topicsArr : [CategoryList]!
    var filterArr : [CategoryList]!
    var topicDictArray : [String : [CategoryList]]!
    var filterDictArray = [String : [CategoryList]]()
    var indexTitles = [String]()
    var isFilter = false
//["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    
    @IBOutlet var searchtxt: UITextField!
    @IBOutlet weak var searchbartv: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        searchbartv.delegate = self
        searchbartv.dataSource = self
        searchtxt.setIcon(#imageLiteral(resourceName: "search (2)"))
        searchbartv.register(UINib(nibName: "Topicscell", bundle: nil), forCellReuseIdentifier: "Topicscell")
        navigationController?.SetupNavWithLeftRightImage(onVc: self, WithTitle: "", leftImage: #imageLiteral(resourceName: "back"), RightImage: nil, leftAction: #selector(back), Image: nil)
        let navLabel = UILabel()
        let navTitle = NSMutableAttributedString(string: "Topics", attributes:[
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "Ubuntu", size: 23)!])
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
       searchtxt.delegate = self
       searchtxt.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)
       searchtxt.appendTextfield()
            // let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
           // view.addGestureRecognizer(tap)
           // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        searchtxt.text = ""
        isFilter = false
        apiGetCategorylist()
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    @objc func searchWorkersAsPerText(_ textfield:UITextField) {
//        filterArr = textfield.text!.isEmpty ? topicsArr : topicsArr.filter({ (item:CategoryList) -> Bool in
//            return item.name.lowercased().range(of: textfield.text!, options: .caseInsensitive, range: nil, locale: nil) != nil
//        })
               filterArr = [CategoryList]()
               filterArr.removeAll()
               if !textfield.text!.isEmpty {
                       isFilter = true
                       let upperCasedStr =  textfield.text!.uppercased()
                       filterDictArray =  topicDictArray.filter{
                           $0.key == String(upperCasedStr.first!)
                       }
                     if let contactfilt = filterDictArray[String(upperCasedStr.first!)]{
                       filterArr = contactfilt.filter{ $0.name.range(of: textfield.text!, options: [.caseInsensitive]) != nil
                    }
                       }
                   }
                   else{
                       isFilter = false
                   }
        searchbartv.reloadData()
    }
    @objc func back()
    {
            self.navigationController?.popViewController(animated: true)
    }
    func createTopicData() {
        topicDictArray = [String: [CategoryList]]()
        topicDictArray.removeAll()
        indexTitles.removeAll()
        for topics in topicsArr {
            let topicKey = String(topics.name.first!)
            if var topicValue = topicDictArray[topicKey] {
                topicValue.append(topics)
                topicDictArray[topicKey] = topicValue
            } else {
                topicDictArray[topicKey] = [topics]
            }
        }
        indexTitles = [String](topicDictArray.keys)
        indexTitles = indexTitles.sorted(by: {$0 < $1})
        print(indexTitles)
        
    }
    func apiGetCategorylist(){
        topicsArr = [CategoryList]()
        topicsArr.removeAll()
        let category_id = UserDefaults.standard.object(forKey: "category_id")
        let params = ["category_id":category_id!]
           ServiceManager.shared.MakeApiCall(ForServiceName: "subCategoryList", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
              // print(response!)
               let status = response?[0]["status"] as? Int ?? 0
               let erro_msg = response?[0]["error_msg"] as? String ?? ""
               if status == 1
               {
                   do {
                       let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                       let decode = try JSONDecoder().decode(CategoryListResponse.self, from: jsonData)
                    self.topicsArr = decode.data!
                    self.filterArr = self.topicsArr
                    self.createTopicData()
                    self.searchbartv.reloadData()
                   } catch let error as NSError {
                       print(error.localizedDescription)
                   }
               }
               else
               {
                   UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
               }
           }, with: nil)
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFilter {
                return filterArr.count
        }
        else
        {
            if topicDictArray == nil {
                return 0
            }
            else
            {
                if let topicKeyValues = topicDictArray[indexTitles[section]]{
                           return topicKeyValues.count
                }
                else
                {
                    return 0
                }

            }
        }
       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = searchbartv.dequeueReusableCell(withIdentifier: "Topicscell") as! Topicscell
        cell.mainView.layer.cornerRadius = cell.mainView.frame.height / 2
        cell.mainView.clipsToBounds = true
        cell.mainView.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
        if isFilter{
            cell.topicnamelbl.text = filterArr[indexPath.row].name
            if filterArr[indexPath.row].name == selectedtopic{
                    cell.changeColor()
                }else{
                    print("No match found")
            }
        }else {
        if let topicKeyValues = topicDictArray[indexTitles[indexPath.section]]{
                cell.topicnamelbl.text = topicKeyValues[indexPath.row].name
            if (topicDictArray[indexTitles[indexPath.section]]![indexPath.row].name == selectedtopic){
                cell.changeColor()
                }else{
                    print("No match found")
            }
            }
        }
        return cell
       }
    func numberOfSections(in tableView: UITableView) -> Int {
         if isFilter {
                   return 1
               }else{
                   if topicDictArray == nil {
                       return 0
                   }
                   else
                   {
                        return indexTitles.count
                   }
                   
               }
    }
   
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isFilter{
            return filterDictArray.keys.first
        }else
        {
            if topicDictArray == nil {
                return ""
            }
            else
            {
                 return indexTitles[section]
            }
        }
    }
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return indexTitles
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFilter{
            selectedtopic = filterArr[indexPath.row].name
             UserDefaults.standard.set(filterArr[indexPath.row].id, forKey: "sub_category_id")
        }else{
        selectedtopic =  topicDictArray[indexTitles[indexPath.section]]![indexPath.row].name
        UserDefaults.standard.set(topicDictArray[indexTitles[indexPath.section]]![indexPath.row].id, forKey: "sub_category_id")
        }
        print(selectedtopic)
        searchbartv.reloadData()
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "BreathingVC") as! BreathingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
extension UITextField {
    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:
            CGRect(x: -5, y: 5, width: 20, height: 20))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 20, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        
        rightView = iconContainerView
        rightViewMode =  .always
        
    }
    
    func appendTextfield(){
        let appendView: UIView = UIView(frame:
        CGRect(x: 20, y: 0, width: 20, height: 20))
        
        leftView = appendView
        leftViewMode = .always
    }
}
