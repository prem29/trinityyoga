//
//  mediaplayervc.swift
//  Trinity_yoga
//
//  Created by Maitree on 30/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class mediaplayervc: UIViewController {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var ivFullScreen: UIImageView!
    @IBOutlet weak var videoPlayer: DGVideoPlayer!
    var timing = Timer()
    var totalSecond : Float!
    var progress : Float = 0.0
    var isPlay = true
    var isResume = true
    var Strchk = ""
    var Email = ""
    var otp = ""
    var isFav = false
    var progressStep : Float = 0.01
    var player = AVPlayer()
    var playercontroller = AVPlayerViewController()
    var indexProgressBar = 0
    var vdoUrl : String!
    var user_id,video_id,like_type : Int!
    var my_like,my_unlike,tot_like,tot_unlike : Int!
    @IBOutlet weak var audiotimer: UILabel!
    @IBOutlet weak var playbutton: UIButton!
    @IBOutlet var AVprogressView: UIProgressView!
    @IBOutlet weak var btnLike: UIButton!
    var topVdoArr = [Video]()
    @IBOutlet weak var btnDislike: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        user_id = UserDefaults.standard.object(forKey: LoginuserId) as? Int
        if isFav{
            btnLike.isHidden = true
            btnDislike.isHidden = true
        }
        self.navigationController?.isNavigationBarHidden = true
        AVprogressView.progress = 0.0
        AVprogressView.transform = AVprogressView.transform.scaledBy(x: 1, y: 3)
        AVprogressView.layer.cornerRadius = AVprogressView.frame.height / 2
        AVprogressView.clipsToBounds = true
        AVprogressView.progressTintColor = UIColor(red: 83/255, green: 208/255, blue: 210/255, alpha: 1)

        let videopathurl = URL(string : vdoUrl)
        let asset = AVURLAsset(url: videopathurl!, options: nil)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        do {
            ivFullScreen.image = try UIImage(cgImage: generator.copyCGImage(at: CMTime(seconds: 0, preferredTimescale: 1), actualTime: nil))
                    } catch let e as NSError {
                             print("Error: \(e.localizedDescription)")
                         }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        if isFav == false{
            self.getLikeDislike()
        }
    }
    func apiGetLikeDislike(){
           let params = ["user_id": user_id!,"video_id" : video_id!,"type" : like_type!]
           ServiceManager.shared.MakeApiCall(ForServiceName: "likeAndUnlike", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: false, ShowTrueFalseValue: true, completionHandler: { (response) in
               let status = response?[0]["status"] as? Int ?? 0
               let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                let data = response?[0]["data"] as? [[String : Any]] ?? [[:]]
                self.my_like = data[0]["mylike"] as? Int
                self.my_unlike = data[0]["myunlike"] as? Int
                self.tot_like = data[0]["totallike"] as? Int
                self.tot_unlike = data[0]["totalunlike"] as? Int
                self.getLikeDislike()
               }else{
                   UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
               }
           }, with: nil)
       }
    func getLikeDislike(){
        if my_like == 1{
            btnLike.setImage(#imageLiteral(resourceName: "fill_like"), for: .normal)
        }else{
             btnLike.setImage(#imageLiteral(resourceName: "likew"), for: .normal)
        }
        if my_unlike == 1{
            btnDislike.setImage(#imageLiteral(resourceName: "fill_dislike"), for: .normal)
        }else{
            btnDislike.setImage(#imageLiteral(resourceName: "dislikew"), for: .normal)
        }
        btnLike.setTitle(String(tot_like), for: .normal)
        btnDislike.setTitle(String(tot_unlike), for: .normal)
    }
    @IBAction func closeaction(_ sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func likeAction(_ sender: Any) {
        if my_like == 0{
            like_type = 1
            apiGetLikeDislike()
        }
        else if my_like == 1{
            like_type = 0
            apiGetLikeDislike()
        }
    }
    @IBAction func dislikeAction(_ sender: UIButton) {
        if my_unlike == 0{
            like_type = 2
            apiGetLikeDislike()
        }
        else if my_unlike == 1{
            like_type = 0
            apiGetLikeDislike()
        }
    }
    @IBAction func playaction(_ sender: UIButton) {
       //self.progress = 0.0
        btnClose.setImage(#imageLiteral(resourceName: "cancel"), for: .normal)
        let videopathurl = URL(string : vdoUrl)
        //print(videopathurl!)
        player = AVPlayer(url: videopathurl!)
        if isResume{
            videoPlayer.player = player
            isPlay = true
        }
        if isPlay {
        videoPlayer.player?.play()
        videoPlayer.player?.externalPlaybackVideoGravity = .resizeAspectFill
        ivFullScreen.isHidden = true
        self.playbutton.setImage(#imageLiteral(resourceName: "pause-3"), for: .normal)
        player.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: nil) { (CMTime) in
        let timeNow = Int(self.videoPlayer.player!.currentTime().value) / Int(self.videoPlayer.player!.currentTime().timescale)
                                let currentMins = timeNow / 60
                                let currentSec =  String(format: "%02d",Int(timeNow % 60))
                                let duration: String = "\(currentMins):\(currentSec)"
                                self.audiotimer.text = duration
                                let timeduration = CMTimeGetSeconds((self.videoPlayer.player?.currentItem!.duration)!)//total time
                                let currentTime = CMTimeGetSeconds((self.videoPlayer.player?.currentItem!.currentTime())!)//playing time
                                self.AVprogressView.progress = Float(currentTime / timeduration)
                                self.progress = self.AVprogressView.progress
                        }
                            isResume = false
                            self.isPlay = false
                        }
                        else{
                            //print(self.progress)
                            self.videoPlayer.player?.pause()
                            self.playbutton.setImage(#imageLiteral(resourceName: "play-button"), for: .normal)
                            self.AVprogressView.progress = self.progress
                            AVprogressView.layer.removeAllAnimations()
                            isPlay = true
                      }
                NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)

        }
    @objc func playerDidFinishPlaying(note: NSNotification) {
    print("Video Finished")
        isResume = true
        self.playbutton.setImage(#imageLiteral(resourceName: "play-button"), for: .normal)
    }
}
