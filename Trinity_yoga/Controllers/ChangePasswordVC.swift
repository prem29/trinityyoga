//
//  ChangePasswordVC.swift
//  Trinity_yoga
//
//  Created by DECODER on 31/10/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class ChangePasswordVC: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var stkOldPass: UIStackView!
    @IBOutlet weak var lblChange: UILabel!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var tfOldPass: SkyFloatingLabelTextField!
    @IBOutlet weak var tfConfirmPass: SkyFloatingLabelTextField!
    @IBOutlet weak var tfNewPass: SkyFloatingLabelTextField!
    var isSet = true
    let user_id = UserDefaults.standard.object(forKey: LoginuserId) as? Int
    override func viewDidLoad() {
        super.viewDidLoad()
        btnChange.applyRadialGradient(colours: [buttoncolor,buttoncolor2])
        btnChange.clipsToBounds = true
        btnChange.setTitleColor(UIColor.white, for: .normal)
        btnChange.layer.cornerRadius = btnChange.frame.height / 2
        if isSet == false{
            stkOldPass.isHidden = true
            lblChange.text = "Reset"
            btnChange.setTitle("RESET", for: .normal)
        }
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGasture))
        swipeLeft.direction = .right
        self.view.addGestureRecognizer(swipeLeft)
    }
    @objc func handleGasture(gesture : UISwipeGestureRecognizer){
        if gesture.direction == .right{
            self.navigationController?.popViewController(animated: true)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfOldPass{
            tfNewPass.becomeFirstResponder()
        }else if textField == tfNewPass{
            tfConfirmPass.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
     func toValidateChangePass()
        {
            if tfOldPass.text!.isEmpty
            {
                UIAlertController().Simplealert(withTitle: "Please enter old password.", Message: "", presentOn: self)
            }
            else if tfNewPass.text!.isEmpty
            {
                UIAlertController().Simplealert(withTitle: "Please enter new password.", Message: "", presentOn: self)
            }else if (tfNewPass.text!.isValidPassword == false){
                UIAlertController().Simplealert(withTitle: "Password are at least 8 character long and contain at least one upercase & one special character.", Message: "", presentOn: self)
            }
            else if tfConfirmPass.text!.isEmpty
            {
                UIAlertController().Simplealert(withTitle: "Please enter confirm password", Message: "", presentOn: self)
            }
            else if tfConfirmPass.text != tfNewPass.text{
                UIAlertController().Simplealert(withTitle: "Password not match", Message: "", presentOn: self)
            }
            else{
                apiChangePass()
            }
    }
    func toValidateSetPass()
          {
              if tfNewPass.text!.isEmpty
              {
                  UIAlertController().Simplealert(withTitle: "Please enter new password.", Message: "", presentOn: self)
              }
              else if (tfNewPass.text!.isValidPassword == false){
                  UIAlertController().Simplealert(withTitle: "Password are at least 8 character long and contain at least one upercase & one special character.", Message: "", presentOn: self)
              }
              else if tfConfirmPass.text!.isEmpty
              {
                         UIAlertController().Simplealert(withTitle: "Please enter confirm password", Message: "", presentOn: self)
              }
              else if tfConfirmPass.text != tfNewPass.text{
                UIAlertController().Simplealert(withTitle: "Password not match", Message: "", presentOn: self)
              }
              else{
                  apiSetPassword()
              }
      }
    @IBAction func btnChangePassAction(_ sender: Any) {
        if isSet{
            toValidateChangePass()
        }
        else{
            toValidateSetPass()
        }
    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func apiChangePass(){
        let  params = ["user_id" : user_id!,"old_password" : tfOldPass.text!, "password" : tfNewPass.text!, "confirm_password" : tfConfirmPass.text!] as [String : Any]
               print(params)
                 ServiceManager.shared.MakeApiCall(ForServiceName: "change_password", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .post, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
           
                      print(response!)
                      let status = response?[0]["status"] as? Int ?? 0
                      let erro_msg = response?[0]["error_msg"] as? String ?? ""
                      if status == 1
                      {
                        let vc = self.storyboard?.instantiateViewController(identifier: "AccountVC")as! AccountVC
                               self.navigationController?.pushViewController(vc, animated: true)
                      }
                      else
                      {
                          UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
                      }
                  }, with: nil)
    }
    func apiSetPassword(){
        let  params = ["user_id" : user_id!,"password" : tfNewPass.text!, "confirm_password" : tfConfirmPass.text!] as [String : Any]
                    print(params)
                      ServiceManager.shared.MakeApiCall(ForServiceName: "reset_password", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .post, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
                
                           print(response!)
                           let status = response?[0]["status"] as? Int ?? 0
                           let erro_msg = response?[0]["error_msg"] as? String ?? ""
                           if status == 1
                           {
                             let vc = self.storyboard?.instantiateViewController(identifier: "LoginVC")as! LoginVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                           }
                           else
                           {
                               UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
                           }
                       }, with: nil)
    }
}
