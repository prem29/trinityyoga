//
//  HomeVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//
import UIKit
import AVKit
import AVFoundation

class HomeVC: UIViewController, UITableViewDataSource,UITableViewDelegate,CustomDelegate{
    @IBOutlet weak var lblTopVdoDesc: UILabel!
    @IBOutlet weak var lblTopVdoTitle: UILabel!
    @IBOutlet var topVideobannerLabel: UILabel!
    @IBOutlet weak var playbtn: UIButton!
    @IBOutlet weak var tablehometv: UITableView!
    var videoArr = [HomeData]()
    var topVdoArr = [Video]()
    var section : Int!
    let user_id = UserDefaults.standard.object(forKey: LoginuserId) as? Int
    var video_id : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        topVideobannerLabel.clipsToBounds = true
        topVideobannerLabel.layer.cornerRadius = 8
        topVideobannerLabel.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        tablehometv.delegate = self
        tablehometv.dataSource = self
        tablehometv.register(UINib(nibName: "Homecell", bundle: nil), forCellReuseIdentifier: "Homecell")
       
}
    override func viewWillAppear(_ animated: Bool) {
        apiGetHomeFeed()
    }
        func apiGetHomeFeed(){

            let params = ["user_id" : user_id!]
            ServiceManager.shared.MakeApiCall(ForServiceName: "home_Feeds", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
                let status = response?[0]["status"] as? Int ?? 0
                let error_msg = response?[0]["error_msg"] as? String ?? ""
                if status == 1{
                    do{
                        let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                        let decode = try JSONDecoder().decode(HomeResponse.self, from: jsonData)
                        self.topVdoArr = decode.data?[0].top_video ?? []
                        self.videoArr = decode.data!
                        self.tablehometv.reloadData()
                        
                    }catch let error as NSError{
                        print(error.localizedDescription)
                    }
                }else{
                    UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
                }
            }, with: nil)
        }// Do any additional setup after loading the view.
    override func viewDidAppear(_ animated: Bool) {
        self.title = "Home"
        let navLabel = UILabel()
        let navTitle = NSMutableAttributedString(string: "Home", attributes:[
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "Ubuntu", size: 23)!])
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
    }
    func apiAddToRecentview(){
        let params = ["user_id": user_id!,"video_id" : video_id!]
            ServiceManager.shared.MakeApiCall(ForServiceName: "addToRecentlyViewed", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
               // print(response!)
                let status = response?[0]["status"] as? Int ?? 0
                let error_msg = response?[0]["error_msg"] as? String ?? ""
                if status == 1{
                    self.apiGetHomeFeed()
                }else{
                    UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
                }
            }, with: nil)
        }
    override func viewDidDisappear(_ animated: Bool) {
        if self.tabBarController?.selectedIndex != 0{
            self.title = ""
        }
        super.viewDidDisappear(animated)
       // player.replaceCurrentItem(with: nil)
    }
       @objc func sidemen()
        {
            self.sideMenuController?.showLeftView(animated: true)
        }
    @IBAction func playingaction(_ sender: UIButton) {
        video_id = topVdoArr[sender.tag].id
        apiAddToRecentview()

        let vc = self.storyboard?.instantiateViewController(identifier: "mediaplayervc")as! mediaplayervc
        self.navigationController?.isNavigationBarHidden = true
        let urlStr = topVdoArr[sender.tag].video
        vc.vdoUrl = videoUrl + urlStr!
        vc.video_id = topVdoArr[sender.tag].id
        vc.my_like = topVdoArr[sender.tag].mylike
        vc.my_unlike = topVdoArr[sender.tag].myunlike
        vc.tot_like = topVdoArr[sender.tag].totallike
        vc.tot_unlike = topVdoArr[sender.tag].totalunlike
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func didSelectItem(id: Int!, video: String!, mylike: Int!, myunlike: Int!, totlike: Int!, totunlike: Int!) {
           let vc = self.storyboard?.instantiateViewController(identifier: "mediaplayervc")as! mediaplayervc
           self.navigationController?.isNavigationBarHidden = true
           vc.vdoUrl = videoUrl + video
           vc.video_id = id
           vc.my_like = mylike
           vc.my_unlike = myunlike
           vc.tot_like = totlike
           vc.tot_unlike = totunlike
           self.navigationController?.pushViewController(vc, animated: true)
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        videoArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tablehometv.dequeueReusableCell(withIdentifier: "Homecell") as! Homecell
        cell.context = self
        cell.delegate = self
       // cell.homecollection.reloadData()
       // cell.homecollection.layoutIfNeeded()
        cell.apiGetHomeFeed()
        self.lblTopVdoTitle.text = topVdoArr[indexPath.row].title
        self.lblTopVdoDesc.text = topVdoArr[indexPath.row].description
    
        if indexPath.section == 0{
            cell.sections = 1
            return cell
        }
        if indexPath.section == 1 {
            cell.sections = 2
            return cell
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 250
        }
        if indexPath.section == 1{
            return 250
        }
        return 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let HeaderView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: UIScreen.main.bounds.width, height: 40))
        
        var HeaderTitle : String = "default"
        let userName = UserDefaults.standard.object(forKey: name) as! String
        if section == 0{
            HeaderTitle = "Recomended for " + userName
        }else{
            HeaderTitle = "New in Trinity Yoga"
        }
        let attrText = NSAttributedString(string: HeaderTitle, attributes: [NSAttributedString.Key.font : UIFont(name: "Ubuntu", size: 23)!, NSAttributedString.Key.foregroundColor : UIColor.black])
        label.attributedText = attrText
        HeaderView.backgroundColor = UIColor.white
        HeaderView.addSubview(label)
        return HeaderView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}
