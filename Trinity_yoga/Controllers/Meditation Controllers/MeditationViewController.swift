//
//  MeditationViewController.swift
//  Trinity_yoga
//
//  Created by Dhruv Govani on 09/04/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
struct MeditationResponse : Decodable{
    let error_msg: String!
    let data: [MeditationData]!
    let status: Int!
}
struct MeditationData : Decodable{
    let id: Int?
    let title, description, video: String!
    let status: Int!
    let myfav : Int!
    let ename : String!
}
class MeditationViewController: UIViewController {
    @IBOutlet var watchNowButton: UIButton!
    @IBOutlet var videoDurationLabel: UILabel!
    @IBOutlet var bannerWidth: NSLayoutConstraint!
    @IBOutlet var bannerView: UIView!
    @IBOutlet var addtofavWidth: NSLayoutConstraint!
    @IBOutlet var watchNowWidth: NSLayoutConstraint!
    @IBOutlet var addtoFavbutton: UIButton!
    @IBOutlet var todaysVideoView: UIView!
    @IBOutlet weak var lblEname: UILabel!
    @IBOutlet weak var lblVideoTitle: UILabel!
    
    @IBOutlet weak var lblDescription: UITextView!
    var user_id,video_id,fav_status : Int!
    var player = AVPlayer()
    var playercontroller = AVPlayerViewController()
    var meditationArr = [MeditationData]()

    var isFav = true
    override func viewDidLoad() {
        super.viewDidLoad()
         user_id = UserDefaults.standard.object(forKey: LoginuserId) as? Int
    
        videoDurationLabel.backgroundColor = UIColor(red: 98/255, green: 197/255, blue: 198/255, alpha: 1)
        
        bannerView.backgroundColor = UIColor(red: 203/255, green: 244/255, blue: 247/255, alpha: 1).withAlphaComponent(0.7)
        bannerView.clipsToBounds = true
        bannerView.layer.cornerRadius = 8
        bannerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]

        bannerWidth.constant = UIScreen.main.bounds.width / 2
        todaysVideoView.backgroundColor = UIColor(red: 243/255, green: 244/255, blue: 247/255, alpha: 1)
        
        addtofavWidth.constant = UIScreen.main.bounds.width / 2 - 20
        watchNowWidth.constant = UIScreen.main.bounds.width / 2 - 20
        
        watchNowButton.clipsToBounds = true
        watchNowButton.layer.cornerRadius = watchNowButton.frame.height / 2
        watchNowButton.layer.shadowColor = UIColor.lightGray.cgColor
        watchNowButton.layer.shadowOffset = CGSize(width: 0, height: 1)
        watchNowButton.layer.shadowRadius = 3
        watchNowButton.layer.shadowOpacity = 1
        watchNowButton.layer.masksToBounds = false;
        watchNowButton.backgroundColor = UIColor(red: 98/255, green: 197/255, blue: 198/255, alpha: 1)

        addtoFavbutton.clipsToBounds = true
               addtoFavbutton.layer.cornerRadius = watchNowButton.frame.height / 2
               addtoFavbutton.layer.shadowColor = UIColor.lightGray.cgColor
               addtoFavbutton.layer.shadowOffset = CGSize(width: 0, height: 1)
               addtoFavbutton.layer.shadowRadius = 3
               addtoFavbutton.layer.shadowOpacity = 1
               addtoFavbutton.layer.masksToBounds = false;
        addtoFavbutton.backgroundColor = UIColor.white
        addtoFavbutton.setTitleColor(UIColor(red: 98/255, green: 197/255, blue: 198/255, alpha: 1), for: .normal)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        apiGetMeditationVdo()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.title = "Meditation"

        let navLabel = UILabel()
        let navTitle = NSMutableAttributedString(string: "Meditation", attributes:[
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "Ubuntu", size: 23)!])
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
    }
    override func viewDidDisappear(_ animated: Bool) {
        if self.tabBarController?.selectedIndex != 4{
            self.title = ""
        }
    }
    func apiAddToFav(){
        let params = ["user_id": user_id!,"video_id" : video_id!, "status" : fav_status!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "favouriteAndUnfavourite", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            print(response!)
            let status = response?[0]["status"] as? Int ?? 0
            let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                self.apiGetMeditationVdo()
            }else{
                UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
            }
        }, with: nil)
    }
    func apiGetMeditationVdo(){
               let user_id = UserDefaults.standard.object(forKey: LoginuserId) as? Int
               let params = ["user_id" : user_id!]
               ServiceManager.shared.MakeApiCall(ForServiceName: "getMeditationVideos", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
                   let status = response?[0]["status"] as? Int ?? 0
                   let error_msg = response?[0]["error_msg"] as? String ?? ""
                   if status == 1{
                       do{
                           let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                           let decode = try JSONDecoder().decode(MeditationResponse.self, from: jsonData)
                           self.meditationArr = decode.data
                        if self.meditationArr[0].myfav == 1{
                            self.addtoFavbutton.setTitle("Remove From Fav", for: .normal)
                            self.isFav = false
                        }
                        if self.meditationArr[0].myfav == 0{
                            self.addtoFavbutton.setTitle("Add To Favourite", for: .normal)
                            self.isFav = true
                        }
                        self.lblEname.text = self.meditationArr[0].ename
                        self.lblVideoTitle.text = self.meditationArr[0].title
                        self.lblDescription.text = self.meditationArr[0].description
                       }catch let error as NSError{
                           print(error.localizedDescription)
                       }
                   }else{
                       UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
                   }
               }, with: nil)
           }
    @IBAction func addToFavAction(_ sender: UIButton) {
        video_id = meditationArr[sender.tag].id
        if isFav{
            fav_status = 1
            apiAddToFav()
            //addtoFavbutton.setTitle("Remove to Favourite", for: .normal)
            isFav = false
        }else{
            fav_status = 0
            apiAddToFav()
            //addtoFavbutton.setTitle("Add to Favourite", for: .normal)
            isFav = true
        }
    }
    @IBAction func watchNowAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "mediaplayervc")as! mediaplayervc
        self.navigationController?.isNavigationBarHidden = true
        let urlStr = meditationArr[sender.tag].video
        vc.vdoUrl = videoUrl + urlStr!
          vc.isFav = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

