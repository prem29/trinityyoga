//
//  FavouriteViewController.swift
//  Trinity_yoga
//
//  Created by Dhruv Govani on 09/04/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
struct FavouriteResponse : Decodable{
    let status : Int!
    let error_msg : String!
    let data : [FavouriteData]!
}
struct FavouriteData : Decodable{
    let id : Int!
    let title : String!
    let description,videoduration: String!
    let video : String!
    let status : Int!
}
class FavouriteViewController: UIViewController {
    let user_id = UserDefaults.standard.object(forKey: LoginuserId)
    @IBOutlet weak var tbvFavourite: UITableView!
    var favArr = [FavouriteData]()
    var player = AVPlayer()
    var playercontroller = AVPlayerViewController()
    var isPlay = true
    var video_id,fav_status : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        //apiGetFavourite()
        tbvFavourite.dataSource = self
        tbvFavourite.delegate = self
        tbvFavourite.register(UINib(nibName: "TutorialVideoTableViewCell", bundle: nil), forCellReuseIdentifier: "TutorialCell")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        apiGetFavourite()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.title = "Favourites"
        let navLabel = UILabel()
        let navTitle = NSMutableAttributedString(string: "Favourites", attributes:[
        NSAttributedString.Key.foregroundColor: UIColor.black,
        NSAttributedString.Key.font: UIFont(name: "Ubuntu", size: 23)!])
        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
    }
    override func viewDidDisappear(_ animated: Bool) {
        if self.tabBarController?.selectedIndex != 3{
        self.title = ""
        }
    }
    func apiGetFavourite(){
        let params = ["user_id": user_id!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "getUserFavouriteVideos", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            let status = response?[0]["status"] as? Int ?? 0
            let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                    let decode = try JSONDecoder().decode(FavouriteResponse.self, from: jsonData)
                    self.favArr = decode.data!
                    print(self.favArr)
                    self.tbvFavourite.reloadData()
                }catch let error as NSError{
                    
                    print(error.localizedDescription)
                }
            }else{
                self.favArr.removeAll()
                self.tbvFavourite.reloadData()
                UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
            }
        }, with: nil)
    }
    func apiRemoveToFav(){
        let params = ["user_id": user_id!,"video_id" : video_id!, "status" : fav_status!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "favouriteAndUnfavourite", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            print(response!)
            let status = response?[0]["status"] as? Int ?? 0
            let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                self.apiGetFavourite()
            }else{
                UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
            }
        }, with: nil)
    }
    @objc func playAction(_ sender : UIButton){
            let vc = self.storyboard?.instantiateViewController(identifier: "mediaplayervc")as! mediaplayervc
            self.navigationController?.isNavigationBarHidden = true
            let urlStr = favArr[sender.tag].video
            vc.vdoUrl = videoUrl + urlStr!
            vc.video_id = favArr[sender.tag].id
            vc.isFav = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        @objc func removeToFavAction(sender : UIButton){
            video_id = favArr[sender.tag].id
            let cell = tbvFavourite.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? TutorialVideoTableViewCell
            fav_status = 0
            apiRemoveToFav()
            cell?.btnAddToFav.setImage(#imageLiteral(resourceName: "fav"), for: .normal)
        }
}
extension FavouriteViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return favArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbvFavourite.dequeueReusableCell(withIdentifier: "TutorialCell", for: indexPath) as! TutorialVideoTableViewCell
               cell.videoTitle.text = favArr[indexPath.row].title
               cell.videoDescription.text = favArr[indexPath.row].description
               cell.videoDuration.text = favArr[indexPath.row].videoduration
              let urlStr = favArr[indexPath.row].video
            let vdoUrl = videoUrl + urlStr!
            print(vdoUrl)
            let videopathurl = URL(string : vdoUrl)
            let asset = AVURLAsset(url: videopathurl!, options: nil)
            let generator = AVAssetImageGenerator(asset: asset)
            generator.appliesPreferredTrackTransform = true
            do {
                   cell.videoThumbnail.image = try UIImage(cgImage: generator.copyCGImage(at: CMTime(seconds: 0, preferredTimescale: 1), actualTime: nil))
                } catch let e as NSError {
                              print("Error: \(e.localizedDescription)")
            }
               cell.playButton.tag = indexPath.row
               cell.btnFullScreen.isHidden = true
              // cell.btnFullScreen.tag = indexPath.row
               cell.playButton.addTarget(self, action: #selector(playAction), for: .touchUpInside)
              // cell.btnFullScreen.addTarget(self, action:  #selector(FullScreenAction), for: .touchUpInside)
               cell.likeButton.tag = indexPath.row
               cell.deslikeButton.tag = indexPath.row
               cell.likeButton.isHidden = true
               cell.deslikeButton.isHidden = true
               cell.btnAddToFav.setImage(#imageLiteral(resourceName: "fill_fav"), for: .normal)
               cell.btnAddToFav.tag = indexPath.row
               cell.btnAddToFav.addTarget(self, action: #selector(removeToFavAction), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
