//
//  ForgotPassword.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPassword: UIViewController {
    @IBOutlet var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var submitbtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.changeMeToDefaultColor()
        submitbtn.applyRadialGradient(colours: [buttoncolor,buttoncolor2])
        submitbtn.clipsToBounds = true
        submitbtn.layer.cornerRadius = submitbtn.frame.height / 2
        submitbtn.setTitleColor(UIColor.white, for: .normal)
        submitbtn.setTitle("SUBMIT", for: .normal)
        // Do any additional setup after loading the view.
    let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGasture))
        swipeLeft.direction = .right
        self.view.addGestureRecognizer(swipeLeft)
    }
    @objc func handleGasture(gesture : UISwipeGestureRecognizer){
        if gesture.direction == .right{
            self.navigationController?.popViewController(animated: true)
        }
    }
    func toValidate(){
        if emailField.text!.isEmpty
        {
            UIAlertController().Simplealert(withTitle: "Please enter email.", Message: "", presentOn: self)
        }
        else{
            apiForgotPassword()
        }
    }
    func apiForgotPassword(){
           let  params = ["email" : emailField.text!]
           print(params)
              ServiceManager.shared.MakeApiCall(ForServiceName: "forgot_password", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .post, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
                      print(response!)
                      let status = response?[0]["status"] as? Int ?? 0
                      let erro_msg = response?[0]["error_msg"] as? String ?? ""
                      if status == 1
                      {
                        let data = response?[0]["data"] as? [[String:Any]] ?? [[:]]
                        let user_id = data[0]["id"] as? Int ?? 0
                        UserDefaults.standard.set(user_id, forKey: LoginuserId)
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyYourAccountVC") as! VerifyYourAccountVC
                            self.navigationController?.show(vc, sender: nil)
                      }
                      else
                      {
                          UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
                      }
                  }, with: nil)
              
       }
    @IBAction func backction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitactionbtn(_ sender: UIButton) {
    toValidate()
    }
}
