//
//  SliderVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class SliderVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate{
    
    @IBOutlet weak var slidercollectionview: UICollectionView!
    @IBOutlet weak var pagecontrellers: UIPageControl!
    var slidertimer: Timer!
    var sliderimages = [#imageLiteral(resourceName: "slider1"),#imageLiteral(resourceName: "slider2"),#imageLiteral(resourceName: "slider3")]
    var current_page = 0
    var index = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        slidercollectionview.delegate = self
        slidercollectionview.dataSource = self
        
        slidercollectionview.register(UINib(nibName: "Slider_cell", bundle: nil), forCellWithReuseIdentifier: "Slider_cell")
        
        
        pagecontrellers.currentPageIndicatorTintColor = UIColor.white
        pagecontrellers.pageIndicatorTintColor = UIColor.lightGray
        pagecontrellers.numberOfPages = 3
        
        
        
        slidertimer  = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(timeaction), userInfo: nil, repeats: true)
        self.navigationController?.isNavigationBarHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
          self.title = "Favourites"
      }
      override func viewDidDisappear(_ animated: Bool) {
          self.title = ""
      }
    @objc  func timeaction(){
        index = index + 1
        print("TimerIndex:\(index)")
        print(sliderimages.count)
        self.pagecontrellers.currentPage = index
        if index < self.sliderimages.count
        {
            self.slidercollectionview.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .right, animated: true)
        }
        else
        {
            slidertimer.invalidate()
            let vc = storyboard?.instantiateViewController(withIdentifier: "WelcomescreenVC") as! WelcomescreenVC
            self.navigationController?.show(vc, sender: nil)
//            let vc = storyboard?.instantiateViewController(withIdentifier: "DemotutorialVC") as! DemotutorialVC
//            self.navigationController?.show(vc, sender: nil)
//            pagecontrellers.isHidden = true
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliderimages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = slidercollectionview.dequeueReusableCell(withReuseIdentifier: "Slider_cell", for: indexPath) as! Slider_cell
        cell.sliderimage.image = sliderimages[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 60, height: UIScreen.main.bounds.height - 140)
    }
      func scrollViewDidScroll(_ scrollView: UIScrollView) {
          let visibleRect = CGRect(origin: self.slidercollectionview.contentOffset, size: self.slidercollectionview.bounds.size)
          let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
          if let visibleIndexPath = self.slidercollectionview.indexPathForItem(at: visiblePoint) {
              self.pagecontrellers.currentPage = visibleIndexPath.row
            index = visibleIndexPath.row
          }
      }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
