//
//  AccountVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
struct RecentViewResponse : Decodable{
    let status : Int!
    let error_msg : String!
    let data : [RecentViewData]!
}
struct RecentViewData : Decodable{
    let id : Int!
    let title : String!
    let description : String!
    let video : String!
    let status : Int!
    let totallike : Int!
    let totalunlike : Int!
    let mylike : Int!
    let myunlike : Int!
    let videoduration : String!
}
class AccountVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var editProfileButton: UIButton!
    
    @IBOutlet weak var lblName: UILabel!
    var RecentlyViwedItems = [RecentViewData]()
    @IBOutlet weak var accountimg: UIImageView!

    @IBOutlet weak var accounttv: UITableView!
     let user_id = UserDefaults.standard.object(forKey: LoginuserId)
     var tot_like,tot_dislike,my_like,my_unlike : Int!
    var like_type: Int!
    var videoId : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        editProfileButton.layer.cornerRadius = editProfileButton.frame.height / 2
        editProfileButton.clipsToBounds = true
        editProfileButton.backgroundColor = UIColor(red: 168/255, green: 238/255, blue: 236/255, alpha: 1)
        accounttv.delegate = self
        accounttv.dataSource = self
        accounttv.register(UINib(nibName: "TutorialVideoTableViewCell", bundle: nil), forCellReuseIdentifier: "TutorialCell")
        accountimg.layer.cornerRadius = accountimg.frame.height / 2
        accountimg.clipsToBounds = true
        accountimg.layer.borderWidth = 4
        accountimg.layer.borderColor = UIColor(red: 182/255, green: 239/255, blue: 239/255, alpha: 1).cgColor
        
    }
    override func viewWillAppear(_ animated: Bool) {
        apiGetRecentView()
        if UserDefaults.standard.object(forKey: profile)as! String != ""{
        self.lblName.text = UserDefaults.standard.object(forKey: name) as? String
        let userprofile = UserDefaults.standard.object(forKey: profile) as! String
        Utils.setImageFromUrl(imageView: self.accountimg, urlString: profileUrl + userprofile)
        NotificationCenter.default.addObserver(self,selector: #selector(self.getProfile),
                                                           name: Notification.Name("UserProfile"),
                                                          object: nil)
        }else{
             self.lblName.text = UserDefaults.standard.object(forKey: name) as? String
            accountimg.image = #imageLiteral(resourceName: "profile")
        }
       
        self.navigationController?.navigationBar.isHidden = false
    }
    @objc func getProfile(_ notification: Notification) {
        self.lblName.text  = notification.userInfo?["name"] as? String
        let user_profile =  (notification.userInfo?["profile"] as? String)
        Utils.setImageFromUrl(imageView: self.accountimg, urlString: profileUrl + user_profile!)
    }
//    override func viewDidAppear(_ animated: Bool) {
//          self.title = "Account"
//          let navLabel = UILabel()
//          let navTitle = NSMutableAttributedString(string: "Account", attributes:[
//              NSAttributedString.Key.foregroundColor: UIColor.black,
//              NSAttributedString.Key.font: UIFont(name: "Ubuntu", size: 23)!])
//          navLabel.attributedText = navTitle
//          self.navigationItem.titleView = navLabel
//        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "defaultNavigationBar"), for: .default)
//
//      }
      override func viewDidDisappear(_ animated: Bool) {
        if self.tabBarController?.selectedIndex != 1{
            self.title = ""
        }
        super.viewDidDisappear(animated)
      }
    
    @IBAction func btnManageAccAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "editProfile")as! EditProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func apiGetRecentView(){
        let params = ["user_id": user_id!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "getRecentViewedVideo", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            let status = response?[0]["status"] as? Int ?? 0
            let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                    let decode = try JSONDecoder().decode(RecentViewResponse.self, from: jsonData)
                    self.RecentlyViwedItems = decode.data!
                    print(self.RecentlyViwedItems)
                    self.accounttv.reloadData()
                }catch let error as NSError{
                    print(error.localizedDescription)
                }
            }else{
                UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
            }
        }, with: nil)
    }
    func apiGetLikeDislike(){
            let params = ["user_id": user_id!,"video_id" : videoId!,"type" : like_type!]
            ServiceManager.shared.MakeApiCall(ForServiceName: "likeAndUnlike", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
                let status = response?[0]["status"] as? Int ?? 0
                let error_msg = response?[0]["error_msg"] as? String ?? ""
                if status == 1{
                    self.apiGetRecentView()
                }else{
                    UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
                }
            }, with: nil)
        }
    @objc func playAction(_ sender : UIButton){
    let vc = self.storyboard?.instantiateViewController(identifier: "mediaplayervc")as! mediaplayervc
           self.navigationController?.isNavigationBarHidden = true
           let urlStr = RecentlyViwedItems[sender.tag].video
           vc.vdoUrl = videoUrl + urlStr!
           vc.video_id = RecentlyViwedItems[sender.tag].id
           vc.my_like = RecentlyViwedItems[sender.tag].mylike
           vc.my_unlike = RecentlyViwedItems[sender.tag].myunlike
           vc.tot_like = RecentlyViwedItems[sender.tag].totallike
           vc.tot_unlike = RecentlyViwedItems[sender.tag].totalunlike
           self.navigationController?.pushViewController(vc, animated: true)
        }
//        @objc func FullScreenAction(_ sender : UIButton){
//            let urlStr = RecentlyViwedItems[sender.tag].video
//            let vdoUrl = videoUrl + urlStr!
//            print(vdoUrl)
//            let videopathurl = URL(string : vdoUrl)
//            print(videopathurl!)
//            player = AVPlayer(url: videopathurl!)
//            playercontroller.player = player
//            self.present(playercontroller, animated: true, completion: nil)
//        }
        @objc func likeAction(sender : UIButton){
            videoId = RecentlyViwedItems[sender.tag].id
             my_like = RecentlyViwedItems[sender.tag].mylike
                 if my_like == 0{
                     like_type = 1
                     apiGetLikeDislike()
                 }
                 else if my_like == 1{
                     like_type = 0
                     apiGetLikeDislike()
                 }
             
        }
        @objc func dislikeAction(sender : UIButton){
            videoId = RecentlyViwedItems[sender.tag].id
            my_unlike = RecentlyViwedItems[sender.tag].myunlike
            if my_unlike == 0{
                like_type = 2
                apiGetLikeDislike()
            }
            else if my_unlike == 1{
                like_type = 0
                apiGetLikeDislike()
            }
        }
    @objc func sidemen()
     {
         self.sideMenuController?.showLeftView(animated: true)
     }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RecentlyViwedItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = accounttv.dequeueReusableCell(withIdentifier: "TutorialCell", for: indexPath) as! TutorialVideoTableViewCell

              cell.videoTitle.text = RecentlyViwedItems[indexPath.row].title
              cell.videoDescription.text = RecentlyViwedItems[indexPath.row].description
              tot_like = RecentlyViwedItems[indexPath.row].totallike
              tot_dislike = RecentlyViwedItems[indexPath.row].totalunlike
              my_like = RecentlyViwedItems[indexPath.row].mylike
              my_unlike = RecentlyViwedItems[indexPath.row].myunlike
              if my_like == 1{
                  cell.likeButton.setImage(#imageLiteral(resourceName: "fill_like"), for: .normal)
              }else{
                    cell.likeButton.setImage(#imageLiteral(resourceName: "like"), for: .normal)
              }
              if my_unlike == 1{
                  cell.deslikeButton.setImage(#imageLiteral(resourceName: "fill_dislike"), for: .normal)
              }else{
                    cell.deslikeButton.setImage(#imageLiteral(resourceName: "dislike"), for: .normal)
              }
              cell.likeButton.setTitle(String(tot_like), for: .normal)
              cell.deslikeButton.setTitle(String(tot_dislike), for: .normal)
              cell.videoDuration.text = RecentlyViwedItems[indexPath.row].videoduration

        let urlStr = RecentlyViwedItems[indexPath.row].video
                    let vdoUrl = videoUrl + urlStr!
                    print(vdoUrl)
                    let videopathurl = URL(string : vdoUrl)
            let asset = AVURLAsset(url: videopathurl!, options: nil)
            let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
         do {
            cell.videoThumbnail.image = try UIImage(cgImage: generator.copyCGImage(at: CMTime(seconds: 0, preferredTimescale: 1), actualTime: nil))
                   } catch let e as NSError {
                       print("Error: \(e.localizedDescription)")
                   }
              cell.btnFullScreen.isHidden = true
              cell.playButton.tag = indexPath.row
          //    cell.btnFullScreen.tag = indexPath.row
              cell.playButton.addTarget(self, action: #selector(playAction), for: .touchUpInside)
        //      cell.btnFullScreen.addTarget(self, action:  #selector(FullScreenAction), for: .touchUpInside)
              cell.likeButton.tag = indexPath.row
              cell.deslikeButton.tag = indexPath.row
              cell.likeButton.addTarget(self, action: #selector(likeAction), for: .touchUpInside)
              cell.deslikeButton.addTarget(self, action: #selector(dislikeAction), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: accounttv.frame.origin.x, y: accounttv.frame.origin.y, width: accounttv.frame.width, height: 50))
        
        headerView.backgroundColor = UIColor(red: 233/255, green: 234/255, blue: 235/255, alpha: 1)
        
        let label = UILabel()
        label.frame = CGRect.init(x: 20, y: headerView.frame.origin.y, width: headerView.frame.width, height: headerView.frame.height/2)
        
        
        label.textColor = UIColor.black // my custom colour
        headerView.addSubview(label)
        
        let Infolabel = UILabel()
        Infolabel.frame = CGRect.init(x: UIScreen.main.bounds.width - 60, y: headerView.frame.origin.y, width: headerView.frame.width, height: headerView.frame.height/2)
        
        
        Infolabel.textColor = UIColor(red: 18/255, green: 131/255, blue: 133/255, alpha: 1)// my custom colour
        
        if section == 0 {
            headerView.addSubview(label)
            headerView.addSubview(Infolabel)
            
            label.text = "Recently Viewed"
            
            label.font = UIFont.boldSystemFont(ofSize: 15)
            Infolabel.attributedText = NSAttributedString(string: "View all", attributes:
                [.underlineStyle: NSUnderlineStyle.single.rawValue])
            label.font = UIFont(name: "Ubuntu-B", size: 12)
            label.font = UIFont.boldSystemFont(ofSize: 12)
            Infolabel.font = UIFont(name: label.font.fontName, size: 10)
        }
        return headerView
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
