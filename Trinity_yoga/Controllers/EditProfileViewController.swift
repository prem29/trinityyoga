//
//  EditProfileViewController.swift
//  Trinity_yoga
//
//  Created by Dhruv Govani on 09/04/20.
//  Copyright © 2020 DECODER. All rights reserved.
//
import UIKit
import SkyFloatingLabelTextField
import LGSideMenuController
import Alamofire
struct  UpdateProfileResponse : Decodable {
    let status : Int!
    let error_msg : String!
    let data : [UpdateProfileData]!
}
struct UpdateProfileData : Decodable{
    let gender : String!
}
class EditProfileViewController: UIViewController, UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource,UIGestureRecognizerDelegate{
    @IBOutlet var backLabelBtc: UIBarButtonItem!
    @IBOutlet var ProfileImageView: UIImageView!
    @IBOutlet var fullnameField: SkyFloatingLabelTextField!
    @IBOutlet var phoneNumberField: SkyFloatingLabelTextField!
    @IBOutlet var emailField: SkyFloatingLabelTextField!
    @IBOutlet var addressField: SkyFloatingLabelTextField!
    @IBOutlet var genderField: SkyFloatingLabelTextField!
    @IBOutlet var updateButton: UIButton!
    
    var profileArr = [UpdateProfileData]()
    var selectedGender : String?
    var GenderList = ["","Male","Female","Prefer Not to Say"]
    let GenderPickerView = UIPickerView()
    var imagePicker = UIImagePickerController()
    var editId : Int!
    var genderFlag = ""
    var selectedImage = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        editId = UserDefaults.standard.object(forKey: LoginuserId) as? Int ?? 0
        print(editId!)
        apiGetProfile()
        fullnameField.changeMeToDefaultColor()
        phoneNumberField.changeMeToDefaultColor()
        emailField.changeMeToDefaultColor()
        addressField.changeMeToDefaultColor()
        genderField.changeMeToDefaultColor()
        updateButton.clipsToBounds = true
        updateButton.layer.cornerRadius = updateButton.frame.height / 2
        updateButton.backgroundColor = UIColor(red: 91/255, green: 196/255, blue: 197/255, alpha: 1)
        updateButton.setTitleColor(.white, for: .normal)
        genderField.delegate = self
        GenderPickerView.delegate = self
        GenderPickerView.dataSource = self
        genderField.inputView = GenderPickerView
        let toolbar = UIToolbar();
        toolbar.barTintColor = UIColor(red: 168/255, green: 238/255, blue: 236/255, alpha: 1)
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Ok", style: UIBarButtonItem.Style.plain, target: self, action: #selector(donePicker))
        doneButton.tintColor = UIColor.black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPicker))
        cancelButton.tintColor = UIColor.black
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        genderField.inputAccessoryView = toolbar
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
               view.addGestureRecognizer(tap)
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        ProfileImageView.backgroundColor = .clear
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = ["public.image"]
        ProfileImageView.clipsToBounds = true
        ProfileImageView.layer.cornerRadius = ProfileImageView.frame.height / 2
        ProfileImageView.layer.borderWidth = 4
        ProfileImageView.layer.borderColor = UIColor(red: 182/255, green: 239/255, blue: 239/255, alpha: 1).cgColor
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGasture))
        swipeLeft.direction = .right
        self.view.addGestureRecognizer(swipeLeft)
        
    }
    @objc func handleGasture(gesture : UISwipeGestureRecognizer){
        if gesture.direction == .right{
            self.navigationController?.popViewController(animated: true)
            self.navigationController?.navigationBar.isHidden = true
        }
    }
    @IBAction func btnUpdate_click(_ sender: Any) {
        toValidate()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fullnameField{
            phoneNumberField.becomeFirstResponder()
        }else if textField == phoneNumberField{
            emailField.becomeFirstResponder()
        }else if textField == emailField{
            genderField.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    func toValidate()
        {
            if fullnameField.text!.isEmpty
            {
                UIAlertController().Simplealert(withTitle: "Please enter full name.", Message: "", presentOn: self)
            }else if (fullnameField.text!.isvalidName == false){
                    UIAlertController().Simplealert(withTitle: "Incorrect name.", Message: "", presentOn: self)
                }
            if phoneNumberField.text!.isEmpty
            {
                UIAlertController().Simplealert(withTitle: "Please enter phone number.", Message: "", presentOn: self)
            }else if (phoneNumberField.text!.isPhoneNo == false){
                UIAlertController().Simplealert(withTitle: "Incorrect Phone number.", Message: "", presentOn: self)
            }
            else if emailField.text!.isEmpty
            {
                UIAlertController().Simplealert(withTitle: "Please enter email.", Message: "", presentOn: self)
            }else if (emailField.text!.isvalidEmail == false){
                UIAlertController().Simplealert(withTitle: "Incorrect email.", Message: "", presentOn: self)
            }else {
                apiEditProfile()
            }
    }
    func apiEditProfile(){
        selectedImage = ProfileImageView.image!
        if genderField.text == "Male"{
            genderFlag = "M"
        }else{
            genderFlag = "F"
        }
        let  params = ["user_id" : editId!,"name" : fullnameField.text!, "email" : emailField.text!, "phone" : phoneNumberField.text!,"gender": genderFlag] as [String : Any]
        print(params)
        ServiceManager.shared.MakeApiCall(ForServiceName: "update_profile", withParameters: params, withAttachments: [selectedImage], withAttachmentName: nil, UploadParameter: "profile", httpMethod: .post, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            //self.upload()
            let status = response?[0]["status"] as? Int ?? 0
            let erro_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1
            {
                print(response!)
               
                self.apiGetProfile()
                self.navigationController?.popViewController(animated: true)
                
            }
            else
            {
                UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
            }
        }, with: nil)
    }
    func apiGetProfile(){
           let  params = ["id" : editId!]
           ServiceManager.shared.MakeApiCall(ForServiceName: "loginuserDetails", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
               print(response!)
                let status = response?[0]["status"] as? Int ?? 0
                let erro_msg = response?[0]["error_msg"] as? String ?? ""
                if status == 1
                {
                    let data = response?[0]["data"] as? [[String:Any]] ?? [[:]]
                    let user_name = data[0]["name"] as? String ?? ""
                    let phone = data[0]["phone"] as? String ?? ""
                    let email = data[0]["email"] as? String ?? ""
                    let user_profile = data[0]["profile"] as? String ?? ""
                
                    self.fullnameField.text = user_name
                    self.phoneNumberField.text = phone
                    self.emailField.text = email
                    if UserDefaults.standard.object(forKey: profile)as! String != ""{
                        UserDefaults.standard.set(user_profile, forKey: profile)
                        Utils.setImageFromUrl(imageView: self.ProfileImageView, urlString: profileUrl + user_profile )}else{
                        self.ProfileImageView.image = #imageLiteral(resourceName: "profile")
                    }
                    UserDefaults.standard.set(user_name, forKey: name)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserProfile"), object: nil, userInfo: ["name" : user_name,"profile" : user_profile])
                    
                }
                else
                {
                    UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
                }
           }, with: nil)
       }
    
//    override func viewDidDisappear(_ animated: Bool) {
//          self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "defaultNavigationBar"), for: .default)
//    }
//    override func viewDidAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "navbarImage"), for: .default)
//
//    }
    @objc func donePicker(){
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    @objc func cancelPicker(){
        //cancel button dismiss datepicker dialog
        genderField.text = ""
        self.view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return GenderList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return GenderList[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderField.text = GenderList[row]
    }
    @objc func dismissKeyboard(){
           view.endEditing(true)
    }
    @IBAction func btnChangePassAction(_ sender: Any) {

        let vc = storyboard?.instantiateViewController(identifier: "ChangePasswordVC")as! ChangePasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.navigationBar.isHidden = true
    }
    @IBAction func imageTapped(_ sender: Any) {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let Camera = UIAlertAction(title: "Camera", style: .default, handler: {(alert : UIAlertAction!) in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        let Gallary = UIAlertAction(title: "Gallary", style: .default, handler: {(alert : UIAlertAction!) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in
            self.ProfileImageView.backgroundColor = .clear
        } )
        cancelAction.setValue(UIColor.red, forKey: "titleTextColor")
        optionMenu.addAction(Camera)
        optionMenu.addAction(Gallary)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func backTextTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
}

extension EditProfileViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            ProfileImageView.backgroundColor = UIColor.clear
            ProfileImageView.contentMode = .scaleToFill
            ProfileImageView.image = pickedImage
            self.selectedImage = pickedImage
            
        }
        
        dismiss(animated: true, completion: nil)
    }
}
