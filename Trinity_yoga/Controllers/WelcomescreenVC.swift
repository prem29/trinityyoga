//
//  WelcomescreenVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class WelcomescreenVC: UIViewController {

    @IBOutlet weak var Welcomemessagelbl: UILabel!
    @IBOutlet weak var backgroundscreenimg: UIImageView!
    @IBOutlet weak var yogicimg: UIImageView!
    @IBOutlet weak var registerbtn: UIButton!
    @IBOutlet weak var Loginbtn: UIButton!
    @IBOutlet weak var subtitleslbl: UILabel!
    @IBOutlet var registrationButtonHeight: NSLayoutConstraint!
    
    @IBOutlet var logInButtonHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIScreen.main.bounds.height < 700 {
            registrationButtonHeight.constant = 40
            logInButtonHeight.constant = 40
            registerbtn.layer.cornerRadius = 20
            Loginbtn.layer.cornerRadius = 20

        }else{
            Loginbtn.layer.cornerRadius = Loginbtn.frame.height  / 2
            registerbtn.layer.cornerRadius = registerbtn.frame.height  / 2

        }
        
        registerbtn.applyRadialGradient(colours: [buttoncolor,buttoncolor2])
        registerbtn.setTitleColor(.white, for: .normal)
        registerbtn.clipsToBounds = true
        Loginbtn.backgroundColor = .white
        Loginbtn.setTitleColor(buttontitlecolour, for: .normal)
        Loginbtn.clipsToBounds = true
        Loginbtn.layer.borderColor = buttontitlecolour.cgColor
        Loginbtn.layer.borderWidth = 1
        self.navigationController?.isNavigationBarHidden = true
    }
    @IBAction func registrationaction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CreateyourAccountVC") as! CreateyourAccountVC
              self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func Loginaction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        vc.loginFlag = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: [0,1])
    }
    
    func applyRadialGradient(colours: [UIColor]){
        applyRadialGradient(colours: colours, locations: [0,1])
    }
    
    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 0.5, y: 0.5)
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }

    func applyRadialGradient(colours: [UIColor], locations: [NSNumber]?){
        let gradient: CAGradientLayer = CAGradientLayer()
        let context = UIGraphicsGetCurrentContext()
        let endRadius = sqrt(pow(frame.width/2, 2) + pow(frame.height/2, 2))
        let center = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
            gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 0.5, y: 0.5)
        self.layer.insertSublayer(gradient, at: 0)
        context?.drawRadialGradient(gradient as! CGGradient, startCenter: center, startRadius: 0, endCenter: center,endRadius: endRadius, options: CGGradientDrawingOptions.drawsBeforeStartLocation)
    }
    func dropShadow2(view:UIView,color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        view.layer.masksToBounds = false
        view.layer.cornerRadius = view.bounds.height/2
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOpacity = opacity
        view.layer.shadowOffset = offSet
        view.layer.shadowRadius = radius
    }
}


