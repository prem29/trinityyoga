//
//  LoginVC.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import LGSideMenuController
class LoginVC: UIViewController,UITextFieldDelegate {
    @IBOutlet var emailField: SkyFloatingLabelTextField!
    @IBOutlet var passwordField: SkyFloatingLabelTextField!
    @IBOutlet var signupbutton: UIButton!
    @IBOutlet weak var loginbtn: UIButton!
    var loginFlag = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailField.changeMeToDefaultColor()
        passwordField.changeMeToDefaultColor()

        let att = NSMutableAttributedString(string: "Don't have an Account? SIGN UP");
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location: 0, length: 22))
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 91/255, green: 196/255, blue: 197/255, alpha: 1), range: NSRange(location: 22, length: 8))
        signupbutton.setAttributedTitle(att, for: .normal)
        loginbtn.applyRadialGradient(colours: [buttoncolor,buttoncolor2])
        loginbtn.clipsToBounds = true
        loginbtn.setTitleColor(UIColor.white, for: .normal)
        loginbtn.layer.cornerRadius = loginbtn.frame.height / 2
//        loginbtn.setTitle("SUBMIT", for: .normal)
        // Do any additional setup after loading the view.
    let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGasture))
        swipeLeft.direction = .right
        self.view.addGestureRecognizer(swipeLeft)
    }
    @objc func handleGasture(gesture : UISwipeGestureRecognizer){
        if gesture.direction == .right{
             if loginFlag{
                   self.navigationController?.popViewController(animated: true)
                   }else{
                UIApplication.shared.perform(#selector(NSXPCConnection.suspend))
                    exit(0)
                }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField{
            passwordField.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return true
    }
    func toValidate(){
        if emailField.text!.isEmpty
        {
            UIAlertController().Simplealert(withTitle: "Please enter email.", Message: "", presentOn: self)
        }
        else if passwordField.text!.isEmpty
        {
            UIAlertController().Simplealert(withTitle: "Please enter password", Message: "", presentOn: self)
        }else{
            doSaveLoginData()
        }
    }
    func doSaveLoginData() {
        let  params = ["email" : emailField.text!, "password" : passwordField.text!]
        print(params)
          ServiceManager.shared.MakeApiCall(ForServiceName: "login", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .post, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
    
               print(response!)
               let status = response?[0]["status"] as? Int ?? 0
               let erro_msg = response?[0]["error_msg"] as? String ?? ""
               if status == 1
               {
                let data = response?[0]["data"] as? [[String : Any]] ?? [[:]]
                let id = data[0]["id"] as? Int ?? 0
                let user_name = data[0]["name"] as? String ?? ""
                let user_phone = data[0]["phone"] as? String ?? ""
                let user_email = data[0]["email"] as? String ?? ""
                let user_profile = data[0]["profile"] as? String ?? ""
                //let yoga_level_Id = data[0]["yoga_level_all_id"] as? String ?? ""
                UserDefaults.standard.set(id, forKey: LoginuserId)
                UserDefaults.standard.set(user_name, forKey: name)
                UserDefaults.standard.set(user_phone, forKey: phone)
                UserDefaults.standard.set(user_email, forKey: email)
                UserDefaults.standard.set(user_profile, forKey: profile)
                print(user_profile)
                UserDefaults.standard.set(id, forKey: LoginuserId)
    
                if UserDefaults.standard.object(forKey: yoga_level_all_Id) != nil{
                 let sidemenu = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuVC") as! SidemenuVC
                             let app = UIApplication.shared.delegate as! AppDelegate
                             let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTabbar") as! MyTabbar
                             vc.selectedIndex = 0
                             let mainVC = LGSideMenuController.init(rootViewController: vc, leftViewController: sidemenu, rightViewController: nil)
                             mainVC.leftViewWidth = 300
                             app.window?.rootViewController = mainVC
                 //            app.window?.rootViewController = vc
                             app.window?.makeKeyAndVisible()
                }
                else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeafterloginVC") as! WelcomeafterloginVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
               }
               else
               {
                   UIAlertController().Simplealert(withTitle: erro_msg, Message: "", presentOn: self)
               }
           }, with: nil)
    }
    @IBAction func backaction(_ sender: UIButton) {
        if loginFlag{
        self.navigationController?.popViewController(animated: true)
        }else{
            UIApplication.shared.perform(#selector(NSXPCConnection.suspend))
            exit(0)
        }
    }
    @IBAction func loginactionbtn(_ sender: UIButton) {
        toValidate()
    }
    @IBAction func forgotbtnaction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotPassword") as! ForgotPassword
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func signupaction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CreateyourAccountVC") as! CreateyourAccountVC
        self.navigationController?.show(vc, sender: nil)
    }
}
extension SkyFloatingLabelTextField{
    func changeMeToDefaultColor(){
        self.titleColor = UIColor(red: 84/255, green: 190/255, blue: 192/255, alpha: 1)
        self.selectedTitleColor = UIColor(red: 84/255, green: 190/255, blue: 192/255, alpha: 1)
        self.placeholderColor = UIColor(red: 84/255, green: 190/255, blue: 192/255, alpha: 1)
    }
}
