//
//  SplashScreenVC.swift
//  Trinity_yoga
//
//  Created by DECODER on 04/11/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import LGSideMenuController

class SplashScreenVC: UIViewController {

    @IBOutlet weak var ivSplashScreen: UIImageView!
    override func viewDidLoad() {
         super.viewDidLoad()
         self.navigationController?.isNavigationBarHidden = true
      //  apiSpashScreen()
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(callBack), userInfo: nil, repeats: false)
    }
    @objc func callBack(){
        if (UserDefaults.standard.object(forKey: LoginuserId) != nil) && (UserDefaults.standard.object(forKey: yoga_level_all_Id) != nil){
           let yoda_id = UserDefaults.standard.object(forKey: yoga_level_all_Id)
            print(yoda_id!)
            let sidemenu = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuVC") as! SidemenuVC
            let app = UIApplication.shared.delegate as! AppDelegate
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTabbar") as! MyTabbar
            vc.selectedIndex = 0
            let mainVC = LGSideMenuController.init(rootViewController: vc, leftViewController: sidemenu, rightViewController: nil)
            mainVC.leftViewWidth = 300
            app.window?.rootViewController = mainVC
               //                app.window?.rootViewController = vc
            app.window?.makeKeyAndVisible()
        }
        else{
            let vc = storyboard?.instantiateViewController(identifier: "SliderVC")as! SliderVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func apiSpashScreen(){
        ServiceManager.shared.MakeApiCall(ForServiceName: "splashScreen", withParameters: nil, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            
            let status = response?[0]["status"] as? Int ?? 0
            let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                let data = response?[0]["data"] as? [[String:Any]] ?? [[:]]
                let image = data[0]["image"] as? String ?? ""
                print(image)
                Utils.setImageFromUrl(imageView: self.ivSplashScreen, urlString: splashUrl + image)
            }else{
                 UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self)
            }
            
        }, with: nil)
    }
}
            
