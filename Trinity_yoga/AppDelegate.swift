//
//  AppDelegate.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//
import IQKeyboardManagerSwift
import UIKit
import LGSideMenuController
import AVFoundation
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
 var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        IQKeyboardManager.shared.enable = true
        let path = Bundle.main.path(forResource: "v1", ofType: "mp4")
        print(path!)
//        // Override point for customization after application launch.
        if (UserDefaults.standard.object(forKey: LoginuserId) != nil) && (UserDefaults.standard.object(forKey: yoga_level_all_Id) != nil){
           let yoda_id = UserDefaults.standard.object(forKey: yoga_level_all_Id)
           print(yoda_id!)
            let sidemenu = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuVC") as! SidemenuVC
                           let app = UIApplication.shared.delegate as! AppDelegate
                           let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTabbar") as! MyTabbar
                           vc.selectedIndex = 0
                           let mainVC = LGSideMenuController.init(rootViewController: vc, leftViewController: sidemenu, rightViewController: nil)
                           mainVC.leftViewWidth = 300
                           app.window?.rootViewController = mainVC
               //                app.window?.rootViewController = vc
                           app.window?.makeKeyAndVisible()
        }
        else{
           
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SliderVC") as! SliderVC
            self.window?.rootViewController?.present(vc, animated: true, completion: nil)

        }
        return true
    }
}

