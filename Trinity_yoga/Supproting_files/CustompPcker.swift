//
//  CustompPcker.swift
//  Your Home Services
//
//  Created by Jatin gosai on 03/10/20.
//  Copyright © 2020 Dhruv Govani. All rights reserved.
//

import Foundation
import UIKit

protocol CustomePickerDelegate:class {
    func DidSelect(pickerView:UIPickerView,dataAt Index:Int)
    func DidCancel(PickerView:UIPickerView)
}
class customePicker: UIView,UIPickerViewDelegate,UIPickerViewDataSource{
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var btnCancel: UIButton!

    var DataSource = [String](){
        didSet{
            pickerView.reloadComponent(0)
        }
    }
    weak var Delegate:CustomePickerDelegate?
    
    override func awakeFromNib() {
        self.pickerView.delegate = self
        btnOK.addTarget(self, action: #selector(OK(_:)), for: .touchUpInside)
        btnCancel.addTarget(self, action: #selector(Cancel(_:)), for: .touchUpInside)
    }
    
    @objc func Cancel(_ button:UIButton){
        Delegate?.DidCancel(PickerView: self.pickerView)
    }
    
    @objc func OK(_ button:UIButton)
    {
        Delegate?.DidSelect(pickerView: self.pickerView, dataAt: pickerView.selectedRow(inComponent: 0))
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return DataSource.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return DataSource[row]
    }
}
