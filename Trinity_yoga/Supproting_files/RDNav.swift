//
//  RDNav.swift
//  CLM
//
//  Created by DEVIL DECODER on 21/08/18.
//  Copyright © 2018 DECODER. All rights reserved.
//

import UIKit
import LGSideMenuController

extension UINavigationController{
    
    func setupNav(onVC:UIViewController,withTitle:String?,Image:UIImage?,isSideMenu:Bool,showNotification:Bool){
        
        if isSideMenu
        {
            
            let leftItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "menu"), style: .done, target: self, action: #selector(menuSelected(_:)))
            let left = UIBarButtonItem.init()
            left.title = ""
            onVC.navigationItem.backBarButtonItem = left
            onVC.navigationItem.leftBarButtonItem = leftItem
            sideMenuController?.isLeftViewSwipeGestureEnabled = true
            
        }
        else
        {
            
            let button = UIButton(type: .custom)
            let leftimage = #imageLiteral(resourceName: "left-arrow")
            let tintedImage = leftimage.withRenderingMode(.alwaysTemplate)
            button.setImage(tintedImage, for: .normal)
            button.tintColor = UIColor.white
            button.addTarget(self, action: #selector(backButton(_:)), for: .touchUpInside)
            button.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
            let barButton = UIBarButtonItem(customView: button)
            onVC.navigationItem.leftBarButtonItems = [barButton]
            
            
        }
        
        onVC.navigationController?.navigationBar.tintColor = UIColor.white
        onVC.navigationController?.navigationBar.barTintColor = UIColor.yellow
        
        if let image = Image {
            
            let imageView = UIImageView.init(image:image)
            imageView.contentMode = .scaleAspectFill
            onVC.navigationItem.titleView = imageView
        }
        
        if let _ = withTitle
        {
            onVC.navigationItem.title = withTitle
            onVC.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            
            //            onVC.navigationItem.title = withTitle
            //
            //            onVC.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont.init(name: "ROBOTO-BOLD", size: 20)!,NSAttributedString.Key.foregroundColor: UIColor.white]
            //
        }
        
        if showNotification
        {
            
            let leftItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "Search"), style: .done, target: self, action: #selector(openSearch(_:)))
            let left = UIBarButtonItem.init()
            left.title = ""
            onVC.navigationItem.rightBarButtonItem = leftItem
            
        }
        
    }
    
    
    func setupNavtitle(onVC:UIViewController,withTitle:String?,Image:UIImage?,isSideMenu:Bool,showNotification:Bool){
        
        
        if isSideMenu
        {
            
            let leftItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "menu"), style: .done, target: self, action: #selector(menuSelected(_:)))
            let left = UIBarButtonItem.init()
            left.title = ""
            onVC.navigationItem.backBarButtonItem = left
            onVC.navigationItem.leftBarButtonItem = leftItem
            sideMenuController?.isLeftViewSwipeGestureEnabled = true
            
        }
        else
        {
            
            let longTitleLabel = UILabel()
            longTitleLabel.text = withTitle
            longTitleLabel.textColor = UIColor.white
            longTitleLabel.font = UIFont.init(name: "Swiss721BoldBT", size: 25)
            longTitleLabel.sizeToFit()
            let lefttitleItem = UIBarButtonItem(customView: longTitleLabel)
            onVC.navigationItem.leftBarButtonItems = [lefttitleItem]
        }
        
        onVC.navigationController?.navigationBar.tintColor = UIColor.white
        onVC.navigationController?.navigationBar.barTintColor = UIColor.yellow
        
        if let image = Image {
            
            let imageView = UIImageView.init(image:image)
            imageView.contentMode = .scaleAspectFill
            onVC.navigationItem.titleView = imageView
        }
        
        if showNotification
        {
            
            let leftItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "Search"), style: .done, target: self, action: #selector(openSearch(_:)))
            let left = UIBarButtonItem.init()
            left.title = ""
            onVC.navigationItem.rightBarButtonItem = leftItem
            
        }
        
    }
    
    
    
    func SetupNavWithLeftRightImage(onVc:UIViewController,WithTitle:String?,leftImage:UIImage?,RightImage:UIImage?,leftAction:Selector?,Image:UIImage?)
    {
        
        
        //            let longTitleLabel = UILabel()
        //            longTitleLabel.text = WithTitle
        //            longTitleLabel.textColor = UIColor.white
        //            longTitleLabel.font = UIFont.init(name: "Swiss721BoldBT", size: 20)
        //
        //            longTitleLabel.sizeToFit()
        //            let lefttitleItem = UIBarButtonItem(customView: longTitleLabel)
        //
        onVc.navigationItem.title = WithTitle
//        onVc.navigationItem.navi
        //
        onVc.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
        //
        
        //        if let _ = WithTitle
        //        {
        //            onVc.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont(name: "Swiss721BoldBT", size: 20) as Any]
        //            onVc.navigationItem.title = WithTitle
        //        }
//        SetupNavWithLeftRightImage
        if let rightimage = RightImage { 
            let tintedImage = rightimage.withRenderingMode(.alwaysTemplate)
            let rightbtn = UIButton.init(type: .custom)
            rightbtn.frame = CGRect.init(x: 0 , y: 0, width: 35, height: 40)
            //            rightbtn.setImage(rightimage, for:.normal)
            rightbtn.setImage(tintedImage, for: .normal)
            rightbtn.tintColor = UIColor.black
             rightbtn.addTarget(self, action: #selector(editProfile(_btn:)), for: .touchUpInside)
            
            let lblcount = UILabel.init(frame: CGRect.init(x: 18, y: 0, width: 13, height: 13))
            lblcount.textAlignment = .center
            lblcount.textColor = UIColor.black
            lblcount.layer.cornerRadius = lblcount.frame.height/2
            lblcount.clipsToBounds = true
            lblcount.font = UIFont.init(name: "", size: 9)
            rightbtn.addSubview(lblcount)
            
            let rightbarbtn = UIBarButtonItem.init(customView: rightbtn)
            onVc.navigationItem.rightBarButtonItem = rightbarbtn
        }
        
        if let Leftimage = leftImage {
            let leftbarbtn = UIBarButtonItem.init(image: Leftimage, style: .plain, target: onVc, action: leftAction)
            onVc.navigationItem.leftBarButtonItems = [leftbarbtn]
        }
        //        else
        //        {
        //            onVc.navigationItem.leftBarButtonItems = [lefttitleItem]
        //        }
        onVc.navigationController?.navigationBar.tintColor = UIColor.black
        onVc.navigationController?.navigationBar.barTintColor = UIColor.yellow
        
        if let image = Image {
            
            let imageView = UIImageView.init(image:image)
            imageView.contentMode = .scaleAspectFill
            onVc.navigationItem.titleView = imageView
        }
        
        
    }
    
    @objc private func menuSelected(_ button:UIBarButtonItem){
        sideMenuController?.showLeftViewAnimated()
    }
    
    @objc func backButton(_ button: UIBarButtonItem) {
        //        self.navigationController?.popViewController(animated: true)
        self.popViewController(animated: true)
        
    }
    
    
    @objc func editProfile(_btn : UIBarButtonItem){
        
        
//        let vc = storyboard?.instantiateViewController(withIdentifier: "EditprofileVC") as! EditprofileVC
//        vc.hidesBottomBarWhenPushed = true
//        self.pushViewController(vc, animated: true)
        
    }
    
    @objc func openSearch(_ btn : UIBarButtonItem){
        
        //        guard let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "miscSearchVC") as? miscSearchVC else {return}
        //        vc.hidesBottomBarWhenPushed = true
        //        self.show(vc, sender: nil)
        
    }
    
    
    @objc private func btnAction_cancel(_ button : UIBarButtonItem){
        self.popViewController(animated: true)
    }
    
}
