//
//  GlobalViewShadow.swift
//  Carring
//
//  Created by mackbook on 7/5/16.
//  Copyright © 2017 virtualheight. All rights reserved.
//

import UIKit
func EmptyMessage(message:String, viewController:UIViewController,tblView:UITableView,view:UIView) {
    let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: view.bounds.size.width, height: view.bounds.size.height))
    let messageLabel = UILabel(frame: rect)
    messageLabel.text = message
    messageLabel.textColor = UIColor.black
    messageLabel.numberOfLines = 0
    messageLabel.textAlignment = .center;
    messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
    messageLabel.sizeToFit()
    
    tblView.backgroundView = messageLabel;
    tblView.separatorStyle = .none
}
class GlobalViewShadow: NSObject {
    
    class func ShadowWithoutBorder(_ shadow: UIView,color: UIColor)
    {
        shadow.layer.shadowRadius = 10
        shadow.layer.shadowColor = color.cgColor
        shadow.layer.shadowOffset = CGSize.zero
        shadow.layer.shadowOpacity = 1.0//0.7
        shadow.layer.cornerRadius = 20
    }
    class func ShadowWithBorder(_ BorderShadow: UIView, color : UIColor)
    {
//        BorderShadow.layer.cornerRadius = 5
        BorderShadow.layer.borderWidth = 1
        BorderShadow.layer.borderColor = color.cgColor
//        BorderShadow.layer.shadowRadius = 5
        BorderShadow.layer.shadowColor = color.cgColor
        BorderShadow.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
        BorderShadow.layer.shadowOpacity = 0.5

    }
    class func OnlyShadow(_ shadow: UIView)
    {
        shadow.layer.shadowColor = UIColor.gray.cgColor
        shadow.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        shadow.layer.shadowOpacity = 0.2

    }

}

class globalgradient: NSObject{
    class func gradienter(_ shadow: UIView)
    {
        var gradienteres = CAGradientLayer()
        gradienteres.colors = [UIColor(red: 147/255, green: 220/255, blue: 248/255, alpha: 1),UIColor(red: 78/255, green: 207/255, blue: 255/255, alpha: 1)]
        gradienteres.locations = [0.0 , 1.0]
        gradienteres.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradienteres.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradienteres.frame = shadow.bounds
        shadow.clipsToBounds = true
        shadow.layer.insertSublayer(gradienteres, at: 0)
    }
}
