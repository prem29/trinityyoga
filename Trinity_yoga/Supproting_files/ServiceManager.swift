//
//  ServiceManager.swift
//  CLM
//
//  Created by DEVIL DECODER on 18/08/18.
//  Copyright © 2018 DECODER. All rights reserved.
//

import Foundation
import Alamofire
import Reachability
import MBProgressHUD
class ServiceManager
{
    //Qa URL
    private(set) var Url = "http://365247.online/trinity_yoga/public/"
    private(set) var ClientVersion = "api/v1/"

    private let app = UIApplication.shared.delegate as! AppDelegate
    typealias CompletionHandler = ([[String:Any]]?)->Void
    static var shared:ServiceManager{
        get{
            struct serviceManager {
                static let manager = ServiceManager()
            }
            return serviceManager.manager
        }
    }
    
    let manager:SessionManager!
    
    init() {
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        config.urlCache = nil
        config.timeoutIntervalForRequest = 120
        config.timeoutIntervalForResource = 120
        
        manager = Alamofire.SessionManager(configuration: config)
    }
    
    
    func MakeApiCall(ForServiceName serviceName:String,withParameters Parameters:[String:Any]?,withAttachments Attachments:[Any]?,withAttachmentName:[String]?,UploadParameter:String?,httpMethod:HTTPMethod,ShowLoader:Bool,ShowTrueFalseValue:Bool,ShowRetry:Bool?=false,completionHandler:@escaping CompletionHandler,with RetryMethod:(()->())?)
    {
        let url = "\(Url)\(ClientVersion)\(serviceName)"
        print(url)
        print(ShowRetry)
        var parameters = Parameters
        
//        if UserDefaults.standard.value(forKey: Token) != nil
//        {
//          parameters?["ltoken"] =  UserDefaults.standard.value(forKey: Token) as? String ?? ""
//        }
        print(parameters)
        
        if let attachments = Attachments{
            if (NetworkReachabilityManager()?.isReachable)!{
                if ShowLoader
                {
                    let loadingNotification = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                    loadingNotification.mode = MBProgressHUDMode.indeterminate
                    loadingNotification.label.text = "Loading"
                    //KVNProgress.
                }
                manager.upload(multipartFormData: { (multipart) in
                    for Attachment in attachments
                    {
                        let randome = arc4random()
                        let data = (Attachment as! UIImage).jpegData(compressionQuality: 0.8)
                        multipart.append(data!, withName: UploadParameter!, fileName: "randome\(randome).jpg", mimeType: "image/jpg")
                    }
                    if let params = parameters{
                        
                        for (key,value) in params{
                            multipart.append("\(value)".data(using: String.Encoding.utf8)!, withName: "\(key)")
                        }
                    }
                }, to: URL.init(string: url)!, encodingCompletion: { (Responce) in
                    switch Responce {
                    case .success(let upload, _, _):
                        upload.responseJSON { Responce in
                            if Responce.error == nil
                            {
                                if ShowTrueFalseValue == false
                                {
                                    guard let responce = Responce.result.value else{return}
                                    guard let dic = responce as? [String:Any] else{return}
                                    
                                    if dic["status"] as? Int  == 1{
                                        if ShowLoader{
                                            MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                            guard let data = dic["data"] as? [[String:Any]] else {return}
                                            completionHandler(data)
                                            //                                KVNProgress.dismiss(completion: {
                                            //                                    guard let data = dic["data"] as? [[String:Any]] else {return}
                                            //                                    completionHandler(data)
                                            //                                })
                                        }
                                        else
                                        {
                                            guard let data = dic["data"] as? [[String:Any]] else {return}
                                            completionHandler(data)
                                        }
                                    }
                                    else
                                    {
                                        if ShowLoader{
                                            MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                            
                                            guard let vc = self.app.window?.rootViewController else {return}
                                            RDAlertController.shared.simpleAlert(with: dic["message"] as? String ?? "", message: nil, presentOn: vc)
                                        }
                                        else
                                        {
                                            guard let vc = self.app.window?.rootViewController else {return}
                                            RDAlertController.shared.simpleAlert(with: dic["message"] as? String ?? "", message: nil, presentOn: vc)
                                        }
                                    }
                                }
                                else
                                {
                                    
                                    guard let responce = Responce.result.value else{return}
                                    guard let dic = responce as? [String:Any] else{return}
                                    
                                    if ShowLoader{
                                        MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                        completionHandler([dic])
                                    }
                                    else
                                    {
                                        completionHandler([dic])
                                    }
                                    
                                }
                                
                            }
                            else
                            {
                                if ShowLoader{
                                    MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                    if ShowRetry == false
                                    {
                                        guard let vc = self.app.window?.rootViewController else {return}
                                        RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                                    }
                                }
                                else
                                {
                                    if ShowRetry == false
                                    {
                                        guard let vc = self.app.window?.rootViewController else {return}
                                        RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                                    }
                                    
                                }
                            }
                        }
                    case .failure( _):
                        if ShowLoader{
                            MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                            if ShowRetry == false
                            {
                                guard let vc = self.app.window?.rootViewController else {return}
                                RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                            }
                        }
                        else
                        {
                            if ShowRetry == false
                            {
                                guard let vc = self.app.window?.rootViewController else {return}
                                RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                            }                        }
                    }
                    
                })
            }
            else
            {
                
                guard let vc = app.window?.rootViewController else {return}
                RDAlertController.shared.noInternet(presentOn: vc)
            }
        }
        else
        {
            if (NetworkReachabilityManager()?.isReachable)!{
                if ShowLoader
                {
                    //KVNProgress.show()
                    let loadingNotification = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                   loadingNotification.mode = MBProgressHUDMode.indeterminate
                   loadingNotification.label.text = "Loading"
                }
                
                manager.request(URL.init(string: url)!, method: httpMethod, parameters: parameters).responseJSON(completionHandler: { (Responce) in
                    
                    if Responce.error == nil
                    {
                        if ShowTrueFalseValue == false
                        {
                        guard let responce = Responce.result.value else{return}
                        guard let dic = responce as? [String:Any] else{return}
                        
                        if dic["status"] as? Int  == 1{
                            if ShowLoader{
                                MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                guard let data = dic["data"] as? [[String:Any]] else {return}
                                completionHandler(data)
//                                KVNProgress.dismiss(completion: {
//                                    guard let data = dic["data"] as? [[String:Any]] else {return}
//                                    completionHandler(data)
//                                })
                                }
                            else
                            {
                                guard let data = dic["data"] as? [[String:Any]] else {return}
                                completionHandler(data)
                            }
                        }
//                            else if dic["status"] as? String ?? "" == "5"
//                            {
//                                if ShowLoader{
//                                    MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
//                                    self.SesstionExpired()
//                                }
//                                else
//                                {
//                                   self.SesstionExpired()
//                                }
//
//                            }
                        else
                        {
                            if ShowLoader{
                                MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                
                                guard let vc = self.app.window?.rootViewController else {return}
                                RDAlertController.shared.simpleAlert(with: dic["message"] as? String ?? "", message: nil, presentOn: vc)
//                                KVNProgress.dismiss(completion: {
//                                    guard let vc = self.app.window?.rootViewController else {return}
//                                    RDAlertController.shared.simpleAlert(with: dic["message"] as? String ?? "", message: nil, presentOn: vc)
//                                })
                            }
                            else
                            {
                                guard let vc = self.app.window?.rootViewController else {return}
                                RDAlertController.shared.simpleAlert(with: dic["message"] as? String ?? "", message: nil, presentOn: vc)
                            }
                        }
                        }
                        else
                        {
                            
                                guard let responce = Responce.result.value else{return}
                                guard let dic = responce as? [String:Any] else{return}
                                
                                if ShowLoader{
                                    MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                                    completionHandler([dic])
                                }
                                else
                                {
                                   completionHandler([dic])
                                }
                                
                                
//                                KVNProgress.dismiss(completion: {
//                                    guard let responce = Responce.result.value else{return}
//                                    guard let dic = responce as? [String:Any] else{return}
//                                    completionHandler([dic])
//                                })
                            }
                            
                        }
                    else
                    {
                        if ShowLoader{
                            MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                            if ShowRetry == false
                            {
                            guard let vc = self.app.window?.rootViewController else {return}
                            RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                            }
                        }
                        else
                        {
                            if ShowRetry == false
                            {
                            guard let vc = self.app.window?.rootViewController else {return}
                            RDAlertController.shared.ServerError(presentOn: vc, retry: RetryMethod)
                            }
                            
                        }
                    }
                })
            }
            else
            {
                if ShowRetry == false
                {
                    guard let vc = app.window?.rootViewController else {return}
                    RDAlertController.shared.noInternet(presentOn: vc)
                }
                
            }
        }
    }
    
//    func SesstionExpired()
//    {
//        let app = UIApplication.shared.delegate as! AppDelegate
//
//        let alert = UIAlertController.init(title: "Session Expired", message: "You are login from anyother device", preferredStyle: .alert)
//
//        alert.addAction(UIAlertAction.init(title: "ok", style: .default, handler: { (ok) in
//
//            if User.Shared != nil
//            {
//                User.Shared.LogoutUser(user: User.Shared)
//            }
//
//            guard let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else {return}
//            guard let nav = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TNTNav") as? UINavigationController else {return}
//            nav.navigationBar.isTranslucent = false
//
//            nav.setViewControllers([vc], animated: true)
//            app.window?.rootViewController = nav
//            app.window?.makeKeyAndVisible()
//        }))
//
//        app.window?.rootViewController?.present(alert, animated: true, completion: nil)
//
//    }
//
    
}
