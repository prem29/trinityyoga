//
//  Slider_cell.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class Slider_cell: UICollectionViewCell {

    @IBOutlet var sliderImageTrailing: NSLayoutConstraint!
    @IBOutlet var sliderImageLeading: NSLayoutConstraint!
    @IBOutlet weak var sliderdescription: UILabel!
    @IBOutlet weak var sliderheading: UILabel!
    @IBOutlet weak var sliderimage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIScreen.main.bounds.height < 700{
            sliderImageTrailing.constant = 45
            sliderImageLeading.constant = 45
        }
        
        sliderimage.layer.cornerRadius = 20
        sliderimage.clipsToBounds = true
        // Initialization code
    }

}
