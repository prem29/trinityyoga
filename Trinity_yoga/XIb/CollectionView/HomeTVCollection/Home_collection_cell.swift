//
//  Home_collection_cell.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
class Home_collection_cell: UICollectionViewCell {
    @IBOutlet weak var VideoViewVc: DGVideoPlayer!
    @IBOutlet var likeButton: UIButton!
    @IBOutlet var videoThumbnail: UIImageView!
    @IBOutlet var videoDuration: UILabel!
    @IBOutlet var deslikeButton: UIButton!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var videoDescription: UILabel!
    @IBOutlet var videoTitle: UILabel!
    @IBOutlet weak var btnFullScreen: UIButton!
    @IBOutlet weak var btnAddToFav: UIButton!
    weak var viewController: UIViewController?
    
    @IBOutlet weak var lblNew: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        VideoViewVc.clipsToBounds = true
        lblNew.layer.cornerRadius = 2
        lblNew.clipsToBounds = true
        // Initialization code
    }
}
extension Int {
func shorted() -> String {
    if self >= 1000 && self < 10000 {
        return String(format: "%.1fK", Double(self/100)/10).replacingOccurrences(of: ".0", with: "")
    }
    if self >= 10000 && self < 1000000 {
        return "\(self/1000)k"
    }
    if self >= 1000000 && self < 10000000 {
        return String(format: "%.1fM", Double(self/100000)/10).replacingOccurrences(of: ".0", with: "")
    }
    if self >= 10000000 {
        return "\(self/1000000)M"
    }
    return String(self)
  }
}
