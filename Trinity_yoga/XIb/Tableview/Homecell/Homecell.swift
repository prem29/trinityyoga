//
//  Homecell.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//
import UIKit
import AVKit
import AVFoundation
protocol CustomDelegate: class {
    func didSelectItem(id: Int!,video : String!,mylike : Int!,myunlike : Int!,totlike : Int!,totunlike : Int!)
}
struct HomeResponse: Decodable {
    let error_msg: String!
    let data: [HomeData]!
    let status: Int!
}
struct HomeData: Decodable {
    let top_video, recommended_video, new_video: [Video]!
}
struct Video: Decodable {
    let id: Int?
    let title, description, video: String!
    let status: Int!
    let mylike : Int!
    let myunlike : Int!
    let totallike : Int!
    let totalunlike : Int!
    let videoduration : String!
}
class Homecell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,AVPlayerViewControllerDelegate{
    var delegate : CustomDelegate!
    var recVdoArr = [Video]()
    var newVdoArr = [Video]()
    var isPlay = true
    var user_id,video_id,like_type : Int!
    var sections : Int!
    var arrayFlag = false
    var recVdo_my_like,recVdo_my_unlike,newVdo_my_like,newVdo_my_unlike : Int!
    var recVdo_tot_like,recVdo_tot_dislike,newVdo_tot_like,newVdo_tot_dislike : Int!
    var isFav = true
    var fav_status : Int!
    var context : HomeVC!
    @IBOutlet weak var homecollection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        user_id = UserDefaults.standard.object(forKey: LoginuserId) as? Int
        apiGetHomeFeed()
        homecollection.delegate = self
        homecollection.dataSource = self
        homecollection.register(UINib(nibName: "Home_collection_cell", bundle: nil), forCellWithReuseIdentifier: "Home_collection_cell")
   }
   func apiGetLikeDislike(){
        let params = ["user_id": user_id!,"video_id" : video_id!,"type" : like_type!]
        ServiceManager.shared.MakeApiCall(ForServiceName: "likeAndUnlike", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
            let status = response?[0]["status"] as? Int ?? 0
            let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                    self.apiGetHomeFeed()
            }else{
                UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self.context)
            }
        }, with: nil)
    }
    func apiAddToRecentview(){
        let params = ["user_id": user_id!,"video_id" : video_id!]
            ServiceManager.shared.MakeApiCall(ForServiceName: "addToRecentlyViewed", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
               // print(response!)
                let status = response?[0]["status"] as? Int ?? 0
                let error_msg = response?[0]["error_msg"] as? String ?? ""
                if status == 1{
                    self.apiGetHomeFeed()
                }else{
                    UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self.context)
                }
            }, with: nil)
        }
    func apiAddToFav(){
           let params = ["user_id": user_id!,"video_id" : video_id!, "status" : fav_status!]
           ServiceManager.shared.MakeApiCall(ForServiceName: "favouriteAndUnfavourite", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
              // print(response!)
            let status = response?[0]["status"] as? Int ?? 0
            let error_msg = response?[0]["error_msg"] as? String ?? ""
            if status == 1{
                self.apiGetHomeFeed()
            }else{
                UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self.context)
            }
           }, with: nil)
       }
    func apiGetHomeFeed(){
        let params = ["user_id" : user_id!]
               ServiceManager.shared.MakeApiCall(ForServiceName: "home_Feeds", withParameters: params, withAttachments: nil, withAttachmentName: nil, UploadParameter: nil, httpMethod: .get, ShowLoader: true, ShowTrueFalseValue: true, completionHandler: { (response) in
                   let status = response?[0]["status"] as? Int ?? 0
                   let error_msg = response?[0]["error_msg"] as? String ?? ""
                   if status == 1{
                       do{
                           let jsonData = try JSONSerialization.data(withJSONObject: response?[0] ?? [:], options: [])
                           let decode = try JSONDecoder().decode(HomeResponse.self, from: jsonData)
                        self.recVdoArr = decode.data?[0].recommended_video ?? []
                        self.newVdoArr = decode.data?[0].new_video ?? []
                          // print(self.recVdoArr)
                           self.homecollection.reloadData()
                       }catch let error as NSError{
                           print(error.localizedDescription)
                       }
                   }else{
                    UIAlertController().Simplealert(withTitle: error_msg, Message: "", presentOn: self.context)
                   }
               }, with: nil)
    }
    @objc func newVdeoplayAction(_ sender : UIButton){
        video_id = newVdoArr[sender.tag].id
        apiAddToRecentview()

        let videoid = newVdoArr[sender.tag].id
        let mylike = newVdoArr[sender.tag].mylike
        let myunlike = newVdoArr[sender.tag].myunlike
        let urlStr = newVdoArr[sender.tag].video
        let totlike = newVdoArr[sender.tag].totallike
        let totunlike = newVdoArr[sender.tag].totalunlike
                   
        delegate.didSelectItem(id: videoid!, video: urlStr!, mylike: mylike!, myunlike: myunlike!, totlike: totlike!, totunlike: totunlike!)
    }
    @objc func recVdeoplayAction(_ sender : UIButton){
        video_id = recVdoArr[sender.tag].id
        apiAddToRecentview()
        let videoid = recVdoArr[sender.tag].id
        let mylike = recVdoArr[sender.tag].mylike
        let myunlike = recVdoArr[sender.tag].myunlike
        let urlStr = recVdoArr[sender.tag].video
        let totlike = recVdoArr[sender.tag].totallike
        let totunlike = recVdoArr[sender.tag].totalunlike
            
        delegate.didSelectItem(id: videoid!, video: urlStr!, mylike: mylike!, myunlike: myunlike!, totlike: totlike!, totunlike: totunlike!)
    }
  @objc func recVdolikeAction(sender : UIButton){
               video_id = recVdoArr[sender.tag].id
                recVdo_my_like = recVdoArr[sender.tag].mylike
                    if recVdo_my_like == 0{
                        like_type = 1
                        apiGetLikeDislike()
                    }
                    else if recVdo_my_like == 1{
                        like_type = 0
                        apiGetLikeDislike()
              }
          
           }
  @objc func recVdodislikeAction(sender : UIButton){
               video_id = recVdoArr[sender.tag].id
               recVdo_my_unlike = recVdoArr[sender.tag].myunlike
               if recVdo_my_unlike == 0{
                   like_type = 2
                   apiGetLikeDislike()
               }
               else if recVdo_my_unlike == 1{
                   like_type = 0
                   apiGetLikeDislike()
               }
          
           }
    @objc func newVdolikeAction(sender : UIButton){
                  video_id = newVdoArr[sender.tag].id
                   newVdo_my_like = newVdoArr[sender.tag].mylike
                       if newVdo_my_like == 0{
                           like_type = 1
                           apiGetLikeDislike()
                       }
                       else if newVdo_my_like == 1{
                           like_type = 0
                           apiGetLikeDislike()
                 }
              }
    @objc func newVdodislikeAction(sender : UIButton){
                  video_id = newVdoArr[sender.tag].id
                  newVdo_my_unlike = newVdoArr[sender.tag].myunlike
                  if newVdo_my_unlike == 0{
                      like_type = 2
                      apiGetLikeDislike()
                  }
                  else if newVdo_my_unlike == 1{
                      like_type = 0
                      apiGetLikeDislike()
         }
    }
    //    @objc func addToRecFavAction(sender : UIButton){
    //        video_id = recVdoArr[sender.tag].id
    //         let cell = homecollection.cellForItem(at: IndexPath(row: sender.tag, section: 0)) as? Home_collection_cell
    //        if isFav{
    //            fav_status = 1
    //            apiAddToFav()
    //            cell?.btnAddToFav.setImage(#imageLiteral(resourceName: "fill_fav"), for: .normal)
    //            isFav = false
    //        }else{
    //            fav_status = 0
    //            apiAddToFav()
    //            isFav = true
    //            cell?.btnAddToFav.setImage(#imageLiteral(resourceName: "fav"), for: .normal)
    //        }
    //   }
    //    @objc func addToNewFavAction(sender : UIButton){
    //        video_id = newVdoArr[sender.tag].id
    //         let cell = homecollection.cellForItem(at: IndexPath(row: sender.tag, section: 0)) as? Home_collection_cell
    //        if isFav{
    //            fav_status = 1
    //            apiAddToFav()
    //            cell?.btnAddToFav.setImage(#imageLiteral(resourceName: "fill_fav"), for: .normal)
    //            isFav = false
    //        }else{
    //            fav_status = 0
    //            apiAddToFav()
    //            isFav = true
    //            cell?.btnAddToFav.setImage(#imageLiteral(resourceName: "fav"), for: .normal)
    //        }
    //    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if sections == 1{
            return recVdoArr.count}
        if sections == 2{
            return newVdoArr.count}
        return 0
       }
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = homecollection.dequeueReusableCell(withReuseIdentifier: "Home_collection_cell", for: indexPath) as! Home_collection_cell
        
        if sections == 1{
            cell.videoTitle.text = recVdoArr[indexPath.row].title
            cell.videoDescription.text = recVdoArr[indexPath.row].description
            cell.videoDuration.text = recVdoArr[indexPath.row].videoduration
            recVdo_my_like = recVdoArr[indexPath.row].mylike
            recVdo_my_unlike = recVdoArr[indexPath.row].myunlike
            if recVdo_my_like == 1{
                cell.likeButton.setImage(#imageLiteral(resourceName: "fill_like"), for: .normal)
            }
            else{
                cell.likeButton.setImage(#imageLiteral(resourceName: "like"), for: .normal)
            }
            if recVdo_my_unlike == 1{
                cell.deslikeButton.setImage(#imageLiteral(resourceName: "fill_dislike"), for: .normal)
            }
            else{
                  cell.deslikeButton.setImage(#imageLiteral(resourceName: "dislike"), for: .normal)
            }
            recVdo_tot_like = recVdoArr[indexPath.row].totallike
            recVdo_tot_dislike = recVdoArr[indexPath.row].totalunlike
            cell.likeButton.setTitle(String(recVdo_tot_like), for: .normal)
            cell.deslikeButton.setTitle(String(recVdo_tot_dislike), for: .normal)
            let urlStr = recVdoArr[indexPath.row].video
            let vdoUrl = videoUrl + urlStr!
          //  print(vdoUrl)
            let videopathurl = URL(string : vdoUrl)
            let asset = AVURLAsset(url: videopathurl!)
            let generator = AVAssetImageGenerator(asset: asset)
            generator.appliesPreferredTrackTransform = true
             do {
                cell.videoThumbnail.image = try UIImage(cgImage: generator.copyCGImage(at: CMTime(seconds: 0, preferredTimescale: 1), actualTime: nil))
             } catch let e as NSError {
                        print("Error: \(e.localizedDescription)")
             }
            cell.btnFullScreen.isHidden = true
            cell.playButton.tag = indexPath.row
            //cell.btnFullScreen.tag = indexPath.row
            cell.playButton.addTarget(self, action: #selector(recVdeoplayAction), for: .touchUpInside)
           // cell.btnFullScreen.addTarget(self, action: #selector(recVideofullScreenAction), for: .touchUpInside)
            cell.playButton.tag = indexPath.row
            cell.likeButton.tag = indexPath.row
            cell.deslikeButton.tag = indexPath.row
            cell.likeButton.addTarget(self, action: #selector(recVdolikeAction), for: .touchUpInside)
            cell.deslikeButton.addTarget(self, action: #selector(recVdodislikeAction), for: .touchUpInside)
            cell.lblNew.isHidden = true
            //cell.btnAddToFav.tag = indexPath.row
            //cell.btnAddToFav.addTarget(self, action: #selector(addToRecFavAction), for: .touchUpInside)
            return cell
        }
        if sections == 2{
        cell.videoTitle.text = newVdoArr[indexPath.row].title
        cell.videoDescription.text = newVdoArr[indexPath.row].description
        cell.videoDuration.text = newVdoArr[indexPath.row].videoduration
        newVdo_my_like = newVdoArr[indexPath.row].mylike
        newVdo_my_unlike = newVdoArr[indexPath.row].myunlike
        if newVdo_my_like == 1{
            cell.likeButton.setImage(#imageLiteral(resourceName: "fill_like"), for: .normal)
        }else{
            cell.likeButton.setImage(#imageLiteral(resourceName: "like"), for: .normal)
        }
        if newVdo_my_unlike == 1{
            cell.deslikeButton.setImage(#imageLiteral(resourceName: "fill_dislike"), for: .normal)
        }else{
            cell.deslikeButton.setImage(#imageLiteral(resourceName: "dislike"), for: .normal)
        }
            newVdo_tot_like = newVdoArr[indexPath.row].totallike
            newVdo_tot_dislike = newVdoArr[indexPath.row].totalunlike
            cell.likeButton.setTitle(String(newVdo_tot_like), for: .normal)
            cell.deslikeButton.setTitle(String(newVdo_tot_dislike), for: .normal)
            let urlStr = newVdoArr[indexPath.row].video
            let vdoUrl = videoUrl + urlStr!
           // print(vdoUrl)
            let videopathurl = URL(string: vdoUrl)
            //print(videopathurl!)
            let asset = AVURLAsset(url: videopathurl!)
            let generator = AVAssetImageGenerator(asset: asset)
            generator.appliesPreferredTrackTransform = true
             do {
                cell.videoThumbnail.image = try UIImage(cgImage: generator.copyCGImage(at: CMTime(seconds: 0, preferredTimescale: 1), actualTime: nil))
                } catch let e as NSError {
                    print("Error: \(e.localizedDescription)")
            }
            cell.playButton.tag = indexPath.row
            cell.btnFullScreen.isHidden = true
            //cell.btnFullScreen.tag = indexPath.row
            cell.playButton.addTarget(self, action: #selector(newVdeoplayAction), for: .touchUpInside)
            //cell.btnFullScreen.addTarget(self, action: #selector(newVideofullScreenAction), for: .touchUpInside)
            cell.playButton.tag = indexPath.row
            cell.likeButton.tag = indexPath.row
            cell.deslikeButton.tag = indexPath.row
            cell.likeButton.addTarget(self, action: #selector(newVdolikeAction), for: .touchUpInside)
            cell.deslikeButton.addTarget(self, action: #selector(newVdodislikeAction), for: .touchUpInside)
            //cell.btnAddToFav.tag = indexPath.row
            //cell.btnAddToFav.addTarget(self, action: #selector(addToNewFavAction), for: .touchUpInside)
            return cell
        }
        return cell
}
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let cell = homecollection.dequeueReusableCell(withReuseIdentifier: "Home_collection_cell", for: indexPath) as! Home_collection_cell
        let label = UILabel(frame: CGRect.zero)
        if sections == 1{
          label.text = recVdoArr[indexPath.row].description
            label.sizeToFit()
           //label.numberOfLines = 0
            return CGSize(width: 270, height: cell.videoThumbnail.bounds.height + cell.videoTitle.bounds.height + label.bounds.height + 20 + 20)}
        if sections == 2{
          cell.videoDescription.text = newVdoArr[indexPath.row].description
          return CGSize(width: 270, height: cell.videoDescription.bounds.height + cell.videoThumbnail.bounds.height + cell.videoTitle.bounds.height + 20 + 20)}
        return CGSize(width: 0, height: 0)
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let videoCell = cell as? Home_collection_cell else { return };
        videoCell.VideoViewVc.player?.pause();
        videoCell.VideoViewVc.player = nil;
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
