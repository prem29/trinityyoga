//
//  Topicscell.swift
//  Trinity_yoga
//
//  Created by Mandeeppc on 20/03/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class Topicscell: UITableViewCell {

    @IBOutlet weak var topicnamelbl: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func changeColor(){
        mainView.backgroundColor = UIColor(red: 182/255, green: 239/255, blue: 239/255, alpha: 1)
    }
//
//    func configCell(Topic : String){
//        topicnamelbl.clipsToBounds = true
//              topicnamelbl.layer.cornerRadius = topicnamelbl.frame.height / 2
//              topicnamelbl.backgroundColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1)
//        topicnamelbl.text = "   \(Topic)   "
//    }
    
}
